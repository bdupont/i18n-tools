/* eslint-disable import/no-dynamic-require */
/* eslint-disable global-require */
const baseDir = __dirname;
const absPath = path => `${baseDir}/app/${path}`;
global.include = file => require(absPath(file));

const express = require("express");
const session = require("express-session");
const passport = require("passport");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const tunnel = require("tunnel");
const axios = require("axios").default;

console.log("process.argv = ", process.argv);

if (process.argv.indexOf("noproxy") === -1) {
	const agent = tunnel.httpsOverHttp({
		proxy: {
			host: "proxyeast.aircanada.ca",
			port: 8080,
		},
	});
	axios.defaults.httpsAgent = agent;
	axios.defaults.proxy = false;
}
const app = express();
const port = process.env.PORT || 4545;
const sessionOptions = session({
	secret: "keyboard cat",
	resave: true,
	saveUninitialized: true,
	cookie: {
		maxAge: 3600000,
	},
});
const bodyParserOptions = bodyParser.urlencoded({
	extended: false,
});

// Routes requirements
const routes = {
	// home: require('./app/routes/home'),
	fareHighlights: include("routes/fare-highlights"),
	basicDialogs: include("routes/basic-dialogs"),
	dynamicKeys: include("routes/dynamic-keys"),
	i18nCreate: include("routes/i18n-create"),
	i18nRetrieve: include("routes/i18n-retrieve"),
	i18nCompare: include("routes/i18n-compare"),
	webScrape: include("routes/web-scrape"),
	kiloMapping: include("routes/kilo-mapping"),
	eUpgrade: include("routes/eupgrade-request-windows"),
};

app.use(cookieParser());
app.use(sessionOptions);
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static("public"));
app.use(bodyParserOptions);
app.use(bodyParser.json());

app.set("views", `${__dirname}/views`);
app.set("view engine", "pug");

app.use((req, res, next) => {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
	res.header("Access-Control-Allow-Headers", "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept");
	next();
});

include("modules/passport")(passport);
include("routes/oAuth")(app, passport);

// Routes setup
Object.values(routes).forEach(route => {
	app.use(route);
});

app.listen(port, () => {
	console.log(`Example app listening on port ${port}!`);
});
