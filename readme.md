# i18n Tools

Tools / Parser / Generator for helping with i18n AEM translator keys

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites application

- [Node.js](https://nodejs.org/en/) - V8 or higher
- [Git for Windows](https://git-scm.com/download/win) - Windows

### Prerequisites 

Configure Git proxy - _for internal use_
git config --global http.proxy http://proxyeast.aircanada.ca:8080

The application need to run within the AC network, since some module rely on AEM Translator data (mainly retrival of Translator key and content).

In order to give nodeJS access to the web through the our corporate proxy, the following line need to be run in the command line:

Within Proxy East:

```
set http_proxy=http://proxyeast.aircanada.ca:8080
set https_proxy=http://proxyeast.aircanada.ca:8080
```

Within Proxy West:

```
set http_proxy=http://proxywest.aircanada.ca:8080
set https_proxy=http://proxywest.aircanada.ca:8080
```

### Installing

Clone the current project
```
git clone https://bitbucket.org/bdupont/i18n-tools.git
```

In the _app/config_ folder, copy the file auth-template.json and rename it auth.json. Insert your basic authorization keys ([https://www.blitter.se/utils/basic-authentication-header-generator/](https://www.blitter.se/utils/basic-authentication-header-generator/)) for each environment.

Open a Command Prompt (type cmd in the search field in the Windows Menu) and go to the correct folder. _Note: You can drag and drop the folder path from and open Explorer window_


Run the following the install command from the correct folder
```
npm i 
```

## Run application

Open a Command Prompt (type cmd in the search field in the Windows Menu) and go to the correct folder
```
node index.js
```

### Outside of AC Network
If you're running the application out of AC Network, you need to run the following command:
```
npm run nopoxy
```

## Path for the different app:
- [Dynamics Keys for Fare highlights and ULCC modal box](http://localhost:4545/dynamic-keys)
- [Create i18n CSV file to upload in AEM](http://localhost:4545/i18n-create)
- [AEM Resources Full Dump Keys](http://localhost:4545/i18n-retrieve)
- [Mapping of resources between Redeem flow and money flow](http://localhost:4545/kilo-mapping)
- [eUpgrades Request Window generator](http://localhost:4545/eupgrade-request-windows)
