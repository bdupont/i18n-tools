/* eslint-disable prettier/prettier */
const { promisify } = require("util");
const fs = require("fs-extra");
// const GoogleSpreadsheet = require("google-spreadsheet");

// Config data
// const gdCredentials = include("config/ac-gd.json");
const { editionsKeys, googleDocs } = include("config/app.json");

const writeFile = promisify(fs.writeFile);
const { doubleQuotingQuote, arrayFlat } = include("services/generic-helpers.js");
const { setFileName, createZip, deleteDataFiles } = include("services/files.js")

const i18nCompareExports = {
	parseWorkbook: async (req, res, next) => {	
		const data = {
			"ply" : {},
			"uat" : {},
			"uat2" : {},
			"pssuat" : {},
			"prod" : {},
		};
		const { files } = req;
		if(files) {
			files.forEach((file) => {
				const { filename } = file;
				console.log("fileName = ", filename);
				// res.locals.translators[];
			})
		}
		
	},
	compareData: (req, res, next) => {

	}
};

module.exports = i18nCompareExports;
