// NodeJS Module

const Excel = require("exceljs");

const getXliffData = include("services/get-xliff-data");

const tasksManagement = include("modules/acTasks");
const { aemRetrieve, editionsKeys } = include("config/app.json");

const MAX_XLS_CHAR = 32000;

const setXliffPromises = ({ src }) => {
	const domain = `${aemRetrieve.domain[src]}`;
	let promises = [];
	aemRetrieve.dictionaries.forEach(dictionary => {
		Object.entries(editionsKeys).forEach(([lang, editions]) => {
			const langData = getXliffData(`${domain}${dictionary}/${lang}.dict.xliff`, dictionary, lang, "", src);
			const mappedData = editions.map(edition => getXliffData(`${domain}${dictionary}/${edition.AEMi18n}.dict.xliff`, dictionary, lang, edition.AEMi18n, src));
			// const mappedData = [];
			promises = [...promises, langData, ...mappedData];
		});
	});
	return promises;
};

const processData = promisesData => {
	const setMapping = (arr, compare) => (arr.indexOf(compare) === -1 ? [...arr, compare] : arr);
	const compiledData = {};
	const mapping = {
		edition: [],
		lang: [],
	};
	promisesData.forEach(({ dictionary, data, edition, lang }) => {
		const langEditionKey = edition || lang;
		const mappingKey = edition ? "edition" : "lang";
		// spread to keep previous data, if not an empty object is set
		compiledData[dictionary] = { ...compiledData[dictionary] };

		Object.entries(data)
			.filter(key => !!key)
			.map(([key, val]) => ({
				key,
				val: val ? val.substring(0, MAX_XLS_CHAR) : val,
			}))
			.forEach(({ key, val }) => {
				// spread to keep previous data, if not an empty object is set
				compiledData[dictionary][key] = { ...compiledData[dictionary][key] };
				compiledData[dictionary][key][langEditionKey] = val;
				mapping[mappingKey] = setMapping(mapping[mappingKey], langEditionKey);
			});
	});
	return {
		compiledData,
		editionsMapping: mapping.edition,
		langsMapping: mapping.lang,
	};
};

const createWorkbook = ({ compiledData, langsMapping, editionsMapping }) => {
	const langsEditionsColumnMapping = [...langsMapping, ...editionsMapping];
	const header = ["key", ...langsEditionsColumnMapping];
	const workbook = new Excel.Workbook();
	Object.entries(compiledData).forEach(([dictKey, dict]) => {
		const dictKeySplit = dictKey.split("/");

		const worksheet = workbook.addWorksheet(`${dictKeySplit[dictKeySplit.length - 1]}-${dictKeySplit[dictKeySplit.length - 2]}`);
		worksheet.addRow(header);

		Object.entries(dict).forEach(([key, keyPairing]) => {
			const values = [];
			Object.entries(keyPairing).forEach(([langEdition, value]) => {
				const indexOf = langsEditionsColumnMapping.indexOf(langEdition);
				values[indexOf] = value;
			});
			worksheet.addRow([key, ...values]);
		});
	});
	return workbook;
};

const i18nRetrieve = async task => {
	const promises = setXliffPromises(task);
	try {
		const promisesData = await Promise.all(promises);
		const processedData = processData(promisesData);
		const workbook = createWorkbook(processedData);
		workbook.xlsx.writeFile(`public/downloads/${task.uid}.xlsx`);
		tasksManagement.updateStatus(task.uid, "completed");
		console.log(`${task.uid} = completed`);
	} catch (err) {
		console.log("err = ", err);
	}
};

const acAemTools = {
	i18nRetrieve: async (req, res, next) => {
		if (req.body.src) {
			const { src } = req.body;
			const tasks = tasksManagement.add(src);
			res.send(tasks);
		}
	},
	i18nRetrieveTasks: async (req, res) => {
		try {
			const tasks = await tasksManagement.get();
			res.send(tasks);
		} catch (err) {
			res.send(err);
		}
	},
};

const init = data => {
	tasksManagement.setCB(data);
	tasksManagement.startOnDeck();
};

init(i18nRetrieve);

module.exports = acAemTools;
