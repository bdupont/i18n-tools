const fs = require("fs-extra");

const routings = include("config/routing-templates.json");

const getTasks = () =>
	new Promise(resolve => {
		resolve({});
	});

const downloadFile = (req, res, next) => {
	const _fn = req.query.fn;
	const _file = `public/downloads/${_fn}`;

	fs.ensureFile(_file)
		.then(() => {
			res.download(_file, `${_fn}`, () => {});
		})
		.catch(err => {
			res.contentType("json");
			res.send({
				err: `There was an issue dowloading the file ${_file}. Please try again`,
			});
		});
};
const resSendFile = function(filename) {
	// this = res from express app.get
	this.send({
		filename,
	});
	return true;
};
const resSendError = function(err) {
	// this = res from express app.get
	this.send({
		err,
	});
};
const render = async (req, res) => {
	const routingInfo = routings[req.path];
	const extraDataFunc = eval(routingInfo.extraData);
	let extraData;
	if (extraDataFunc) {
		extraData = await extraDataFunc();
	}
	res.render(routingInfo.template, {
		title: routingInfo.title,
		data: extraData,
	});
};

module.exports = {
	downloadFile,
	resSendFile,
	resSendError,
	render,
};
