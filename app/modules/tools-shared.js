/* eslint-disable prettier/prettier */
const unique = (value, index, self) => self.indexOf(value) === index;

const padNumber = (num, size) => {
	const s = `0000000000000000000000${num}`;
	return s.substr(s.length - size);
};

const escapeRegExp = str => str.replace(/[-[]\/{}()*+?.\^$|]/g, "\\$&");

const JSONtoCSV = (arr, columns, delimiter = ",") => {
	const a = [columns.join(delimiter), ...arr.map(obj => columns.reduce((acc, key) => `${acc}${!acc.length ? "" : delimiter}"${!obj[key] ? "" : obj[key].replace(/[\n\r\t]/gi, "").replace(/["]/gi, "\"\"")}"`, ""))];
	return a.join("\n");
};

const asyncForEach = async (array, callback) => {
	for (let index = 0; index < array.length; index += 1) {
		await callback(array[index], index, array);
	}
};

module.exports = {
	padNumber,
	escapeRegExp,
	JSONtoCSV,
	asyncForEach,
	filter: {
		unique,
	},
};
