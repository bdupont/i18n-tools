/* eslint-disable prettier/prettier */
/* eslint-disable function-paren-newline */
const fs = require("fs");

const get = include("services/get-promise");
const { aemRetrieve, editionsKeys, csvKeysLangOnly } = include("config/app.json");
const getRequestOptions = include("services/get-requests-options");

const env = "prod";

const strip = str => str.replace("-", " ").toLowerCase();

const getJson = async editionsKey => {
	let body = "";
	const url = `${aemRetrieve.domain[env]}/libs/cq/i18n/dict.${editionsKey}.json`;
	const response = await get.http(
		url,
		getRequestOptions({
			src: "AEM",
			env,
		})
	);
	response.on("data", chunk => (body += chunk));
	await response.end;
	return JSON.parse(body);
};

const getLangsData = () => {
	const promises = [];
	Object.keys(editionsKeys).forEach(editionsKey => {
		const json = getJson(editionsKey);
		promises.push(json);
	});
	return promises;
};

const kiloMappings = (req, res) => {
	if (req.file && req.file.destination) {
		fs.readFile(req.file.destination + req.file.filename, "utf-8", async (err, data) => {
			try{
				const kiloResources = JSON.parse(data);
				const mapping = {
					kilo: `${["key", "Mapping"].join(",")}\n`,
					steadyState: `${["Key", "Mapping"].join(",")}`,
					closeEnough: `${["Steady State Key", 	"Kilo Key", "Steady State EN", "Kilo EN"].join(",")}\n`,
					missing: `${["Key", "EN"].join(",")}\n`,
					allLanguages: `\uFEFF${csvKeysLangOnly.join("\,")}\n`,
				};

				const allLanguagesFound = [];
				const responseData = getLangsData();
				// eslint-disable-next-line camelcase
				const [en, fr, es, de, it, zh, ja, ko, zh_TR] = await Promise.all(responseData);

				Object.entries(kiloResources).forEach(([kiloKey, kiloVal]) => {
					console.log("kiloKey = ", kiloKey);
					let foundEqual = false;
					Object.entries(en).forEach(([steadyStateKey, steadyStateVal]) => {
						if (kiloVal === steadyStateVal) {
							foundEqual = true;
							mapping.kilo += `${[kiloKey, steadyStateKey].join(",")}\n`;
							mapping.steadyState += `${[steadyStateKey, kiloKey].join(",")}\n`;
							if(allLanguagesFound.indexOf(kiloKey) === -1){
								mapping.allLanguages += `${[
									kiloKey,
									"",
									`"${en[steadyStateKey]}"`,
									`"${fr[steadyStateKey]}"`,
									`"${es[steadyStateKey]}"`,
									`"${de[steadyStateKey]}"`,
									`"${it[steadyStateKey]}"`,
									`"${zh[steadyStateKey]}"`,
									`"${ja[steadyStateKey]}"`,
									`"${ko[steadyStateKey]}"`,
									`"${zh_TR[steadyStateKey]}"`,
								].join("\,")}\n`;
								allLanguagesFound.push(kiloKey);
							}
						} else {
							const stripkiloVal = strip(kiloVal);
							const stripSteadyStateVal = strip(steadyStateVal);
							if (stripkiloVal === stripSteadyStateVal) {
								foundEqual = true;
								mapping.closeEnough += `${[steadyStateKey, `"${steadyStateVal}"`, kiloKey, `"${kiloVal}"`].join(",")}\n`;
							}
						}
					});
					if (!foundEqual) {
						mapping.missing += `${[kiloKey, `"${kiloVal}"`].join(",")}\n`;
					}
				});

				fs.writeFile("./output/steady-state-to-kilo.csv", mapping.steadyState, "utf8", err => {});
				fs.writeFile("./output/kilo-to-steady-state.csv", mapping.kilo, "utf8", err => {});
				fs.writeFile("./output/no-mapping.csv", mapping.missing, "utf8", err => {});
				fs.writeFile("./output/close-enough.csv", mapping.closeEnough, "utf8", err => {});
				fs.writeFile("./output/all-languages.csv", mapping.allLanguages, "utf8", err => {});

				res.send({});
			} catch (err) {
				console.log("err = ", err);
				res.send({
					error: err,
				});
			}
		});
	}
};

module.exports = {
	kiloMappings,
};
