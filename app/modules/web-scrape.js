const requestPromise = require("request-promise-native");
const fs = require("fs-extra");
const cheerio = require("cheerio");

const { asyncForEach } = include("modules/tools-shared");
const { filter } = include("services/generic-helpers");

const { paths } = include("sources/urls.json");

const get = (path, extraOptions) => {
	const options = {
		uri: `https://www.aircanada.com${path}`,
		...extraOptions,
	};
	return requestPromise(options);
};

const scrapeData = async (req, res, next) => {
	const pagesData = [];
	let arr404 = [];
	const wStream = fs.createWriteStream("output/scrape-images.txt");
	// eslint-disable-next-line prettier/prettier
	await asyncForEach(paths, async (path, i) => {
		try {
			console.log("path = ", path);
			const data = await get(path, {
				transform: body => {
					let uniqueImages = [];
					const images = [];
					const re = /<main[\s\S]+<\/main>/i;
					const main = body.match(re);
					if (main) {
						const $ = cheerio.load(main[0]);
						$("img").each(function() {
							const imgPath = $(this).attr("src") || $(this).attr("data-ng-src") || $(this).attr("ng-src") || "";
							if (imgPath && imgPath.indexOf("{") === -1) {
								images.push(imgPath);
							} else {
								arr404 = arr404.concat([imgPath]);
							}
						});
						$("picture source").each(function() {
							const imgPath = $(this).attr("srcset") || $(this).attr("data-ng-srcset") || $(this).attr("ng-srcset");
							if (imgPath.indexOf("{") === -1) {
								images.push(imgPath);
							} else {
								arr404 = arr404.concat([imgPath]);
							}
						});
						uniqueImages = images.filter(filter.unique);
					}
					return {
						images: uniqueImages,
						url: paths[i],
					};
				},
			});
			pagesData.push(data);
		} catch (err) {
			console.log("err = ", err);
		}
	});

	wStream.write(`{"data": ${JSON.stringify(pagesData, null, 4)}}`);
	wStream.end();
	res.locals.pagesData = pagesData;
	// next();
	res.send({
		data: JSON.stringify(pagesData, null, 4),
	});
};

const retrieveImages = async (req, res) => {
	const pagesData = await fs.readFile("output/scrape-images.txt", "utf-8");
	const data = JSON.parse(pagesData);
	const wStream = fs.WriteStream("output/images.csv");
	wStream.write("Path,Images\n");
	await asyncForEach(data.data, async page => {
		const url = page.url.replace(".html", "");
		try {
			if (page.images.length) {
				wStream.write(`${page.url},"${page.images.join("\n")}"\n`);
				const dir = `/image-dump/${url}`;
				await fs.ensureDir(dir);
				const pagePromises = page.images.map(path => {
					console.log("path = ", path);
					return get(path, {
						encoding: "binary",
					});
				});
				console.log("pagePromises.length = ", pagePromises.length);
				const imagesData = await Promise.all(pagePromises);
				imagesData.forEach((imageData, i) => {
					const fileTree = page.images[i].split("/");
					const fileName = fileTree[fileTree.length - 1];
					fs.writeFile(`${dir}/${fileName}`, imageData, "binary", function(err) {});
				});
			}
		} catch (err) {
			console.log("err = ", err);
		}
	});
	wStream.end();
	res.send();
};

// eslint-disable-next-line no-unused-vars
const scrapeDailog = async (req, res) => {
	const dailogFound = [];
	await asyncForEach(paths, async path => {
		try {
			await get(path, {
				transform: body => {
					const re = /<main[\s\S]+<\/main>/i;
					const main = body.match(re);
					if (main) {
						console.log("path = ", path);
						if (main.indexOf("dailog") !== -1) {
							console.log("Found!");
							dailogFound.push(path);
						}
					}
				},
			});
		} catch (err) {
			console.log("err = ", err);
		}
	});
	console.log(" = ", dailogFound);
};

module.exports = {
	scrapeData,
	retrieveImages,
	scrapeDailog,
};
