/* eslint-disable prettier/prettier */
const { promisify } = require("util");
const fs = require("fs-extra");

const { editionsKeys, googleTabs, csvKeysLangOnly, aemRetrieve, pathToDownload } = include("config/app.json");

const writeFile = promisify(fs.writeFile);
const { SheetDoc } = include("services/google-sheets.js");
const { doubleQuotingQuote, arrayFlat, arrayToObj, filter } = include("services/generic-helpers.js");
const { setFileName, createZip, deleteDataFiles } = include("services/files.js");

let sheetDoc;

const domains = Object.keys(aemRetrieve.domain).concat(["no-env"]);
const createDirectory = async() => {
	const promises = [];
	domains.forEach((domain) => {
		promises.push(fs.ensureDir(`${pathToDownload}${domain}`));
	});
	return Promise.all(promises);
};

const replaceUrlPrefix = (str, domain) => {
	if(domain !== "no-env"){
		const domainPrefix = domain === "prod" ? "www" : domain;
		return str.replace(/\{urlprefix\}/gi, `https://${domainPrefix}.aircanada.com`);
	}
	return str;
};
const replaceSiteEdition = str => str.replace(/\{sitetype\}/gi, "aco");

const parseI18Nsheet = async (sheetId) => {
	const promises = [
		sheetDoc.parseCells(sheetId),
		sheetDoc.headerMapping(sheetId),
	];
	const [data, headerKeys] = await Promise.all(promises);
	const headers = [];
	const strippedHeaders = [];
	headerKeys.forEach((headerKey, headerIndex) => {
		if(csvKeysLangOnly.indexOf(headerKey) !== -1) {
			headers.push(headerKey);
		}else{
			strippedHeaders.push(headerIndex);
		}
	});
	const reversedStrippedHeaders = strippedHeaders.reverse();
	const strippedData = data.map(arr => {
		reversedStrippedHeaders.forEach(headerIndex => {
			arr.splice(headerIndex, 1);
		});
		return arr;
	});
	const i18nData = arrayToObj(strippedData);
	return {
		header: headers.filter(headerKey => headerKey !== "key"),
		i18nData,
	};
};

const getReplaceUrls = async ({ i18nData, header }) => {
	const data = {};
	Object.entries(i18nData).forEach(([key, val]) => {
		const mappedVal = [];
		for(let i = 0, len = val.length; i < len; i += 1){
			mappedVal[i] = typeof val[i] === "undefined" ? "" : val[i];
		}
		const filteredVal = mappedVal
			.map(value => value.indexOf("replaceURL:/") !== -1 ? value : "");
		filteredVal.forEach((value, i) => {
			if(value){
				const lang = header[i];
				if (!data[lang]) {
					data[lang] = {};
				}
				data[lang][key] = value;
			}
		});
	});
	return data;
};

const getI18NStr = data => {
	let content = "";
	Object.entries(data).forEach(([key, val]) => {
		const tempTexts = val
			.join("|||")
			.replace(/"/gi, "\"\"")
			.split("|||")
			.join("\",\"");

		content += `${key},"${tempTexts}"\n`;
	});
	return content;
};

const getLocaleStr = (lang, data)=> {
	let headerLocale = [];
	let contentLocale = [];
	Object.entries(data).forEach(([key, value], index) => {
		let keyContent = [];
		keyContent = [...keyContent, key];
		editionsKeys[lang].forEach(edition => {
			let c = value.replace(/replaceURL:\/parent\//gi, `{urlprefix}/${edition.path.toLowerCase()}/{sitetype}/`);
			if (index === 0) {
				headerLocale = [...headerLocale, edition.AEMi18n];
			}
			c = doubleQuotingQuote(c);
			keyContent = [...keyContent, c];
		});
		contentLocale = [...contentLocale, `"${keyContent.join("\",\"")}"`];
	});
	return {
		headerLocale,
		contentLocale,
	};
};
const generateI18NeditionFile = (sheetKey, replaceUrlData, domain) =>{
	let filesList = [];
	Object.entries(replaceUrlData)
		.filter(([lang]) => lang !== "info" && lang !== "key" )
		.forEach(([lang, data]) => {
			const langKey = lang === "zhtr" || lang === "zh_tr" ? "zh_TR" : lang;
			const { headerLocale, contentLocale } = getLocaleStr(langKey, data);
			const headerLocaleStr = `key,${headerLocale.join(",")}\n`;
			let contentLocaleStr = contentLocale.join("\n");
			const fileInfo = setFileName(`${sheetKey}-${langKey}`, "csv", true, `${domain}/`);
			contentLocaleStr = replaceUrlPrefix(contentLocaleStr, domain);
			contentLocaleStr = replaceSiteEdition(contentLocaleStr);
			writeFile(fileInfo.fullPath, `${headerLocaleStr}${contentLocaleStr}`, "utf-8");
			filesList = filesList.concat([fileInfo]);
		});
	return filesList;
};
const generateI18NLangFile = ({ i18nData, header }, sheetKey, domain) => {
	const headerLangStr = `key,${header.join(",")}\n`;
	let contentLangStr = getI18NStr(i18nData);
	const fileInfo = setFileName(`${sheetKey}`, "csv", true, `${domain}/`);
	contentLangStr = replaceUrlPrefix(contentLangStr, domain);
	writeFile(fileInfo.fullPath, `${headerLangStr}${contentLangStr}`, "utf-8");
	return fileInfo;
};
const getI18nData = async ({ sheetId, title }) => {
	const data = await parseI18Nsheet(sheetId);
	const replaceUrlData = await getReplaceUrls(data);
	let responses = [];
	domains.forEach((domain) => {
		responses= responses.concat(generateI18NeditionFile(title, replaceUrlData, domain), generateI18NLangFile(data, title, domain))
	});
	return responses;
};
const setTabPromises = sheets => {
	const promises = [];
	googleTabs.forEach(googleTab => {
		if(sheets[googleTab]){
			promises.push(getI18nData(sheets[googleTab].sheet));
		}				
	});
	return promises;
};

const parseWorkbook = async (req, res, next) => {
	if (req.body.src && req.body.src.id) {
		try {
			await createDirectory();
			sheetDoc = new SheetDoc(req.body.src.id);
			await sheetDoc.connect();
			sheetDoc.addSheets(sheetDoc.info);
			const { sheets } = sheetDoc;
			const promises = setTabPromises(sheets);
			const filesList = arrayFlat(await Promise.all(promises));
			const uniqueDirectoriesList = filesList
				.map((file) => file.directory)
				.filter(filter.unique);
			const uniqueFilesList = filesList
				.map((file) => file.fullPath)
				.filter(filter.unique);
			const zipFile = await createZip(uniqueDirectoriesList, req.body.src.name);
			deleteDataFiles(uniqueFilesList);

			res.send({
				filename: {
					name: zipFile.name,
				},
			});
		} catch (err) {
			console.log("parseWorkbook error = ", err);
			res.send({
				error: err,
			});
		}
	} else {
		res.send({
			error: "invalid value",
		});
	}
};

module.exports = {
	parseWorkbook,
};
