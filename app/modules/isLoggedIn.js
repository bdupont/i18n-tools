module.exports = (req, res, next) => {
	// if user is authenticated in the session, carry on
	if (req.isAuthenticated()) {
		req.currentUser = req.user;
		return next();
	}
	res.redirect("/");
};
