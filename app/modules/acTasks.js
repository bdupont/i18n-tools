const fs = require("fs-extra");
const { promisify } = require("util");

const writeFile = promisify(fs.writeFile);
const readFile = promisify(fs.readFile);
// const readFile = promisify(fs.readFile);
const { dateStamp } = include("services/dates.js");
const appConfig = include("config/app.json");
const tasks = (() => {
	try {
		return include("tasks/tasks.json");
	} catch (err) {
		fs.outputJSON(appConfig.tasksPath, {});
		return {};
	}
})();

let queue = {};
class Queue {
	constructor() {
		this.onDeck = "";
		this.processData = () => {};
		this.waiting = [];
		this.all = {};
	}
}
const queueHandler = {
	set(obj, name, value) {
		obj[name] = value;
		if (name === "onDeck" && value) {
			obj.processData(obj.onDeck);
		}
		return true;
	},
};

const createUID = src => {
	const dateStampVal = dateStamp(true);
	return `${src}-i18n-${dateStampVal}`;
};

const updateOnDeck = () => {
	if (queue.onDeck) {
		queue.waiting.shift();
		queue.onDeck = null;
	}
	if (queue.waiting.length !== 0) {
		const [first] = queue.waiting;
		queue.onDeck = first;
	}
};

const updateJSON = () => writeFile(appConfig.tasksPath, JSON.stringify(queue.all, null, 4), "utf-8");
const add = src => {
	const uid = createUID(src);
	const date = new Date();
	const task = {
		status: "incomplete",
		date,
		uid,
		src,
	};
	queue.all[uid] = task;
	queue.waiting.push(task);
	updateJSON();
	updateOnDeck();
	return queue.all[uid];
};
const get = () => readFile(appConfig.tasksPath, "utf-8");
const setCB = cb => {
	queue.processData = cb;
};

const callCB = task => {
	queue.processData(task);
};

const requeueIncompleteTasks = tasksList => {
	let waiting = [];
	Object.keys(tasksList).forEach(id => {
		if (tasksList[id].status === "incomplete") {
			waiting = [...waiting, tasksList[id]];
		}
	});
	return waiting;
};

const startOnDeck = () => {
	if (!queue.onDeck && queue.waiting.length) {
		const [first] = queue.waiting;
		queue.onDeck = first;
	}
};

const updateStatus = (uid, status) => {
	queue.all[uid].status = status;
	updateOnDeck();
	updateJSON();
};

const init = process => {
	const queueProxy = new Proxy(new Queue(), queueHandler);
	queueProxy.all = tasks;
	queueProxy.processData = process;
	queueProxy.waiting = requeueIncompleteTasks(tasks);
	return queueProxy;
};

queue = init();

module.exports = {
	add,
	startOnDeck,
	updateStatus,
	get,
	callCB,
	setCB,
};
