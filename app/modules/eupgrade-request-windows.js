/* eslint-disable prettier/prettier */
/* eslint-disable function-paren-newline */
const fs = require("fs-extra");
const { promisify } = require("util");

const writeFile = promisify(fs.writeFile);
// eslint-disable-next-line no-unused-vars
const { googleDocs, csvKeysLangOnly } = include("config/app.json");

const { SheetDoc } = include("services/google-sheets.js");

const { createFile } = include("services/files.js");

const fillEmptyKey = (arr) => {
	const tempArr = [...arr];
	let prevVal = "";
	return tempArr.map((val) => {
		if(val){
			prevVal = val;
		}
		return val || prevVal;
	})
}

const getSheetsData = async (req, res, next) => {
	if(req.body.gdocId) {
		console.log("req.body.gdocId = ", req.body.gdocId);
		const sheetDoc = new SheetDoc(googleDocs[req.body.gdocId]);
		await sheetDoc.connect();
		sheetDoc.addSheets(sheetDoc.info);
		const { sheets } = sheetDoc;

		const promises = [sheetDoc.parseCells(sheets["Request Windows"].id, "eupgrade", true)];
		const data = await Promise.all(promises);
		
		res.locals.sheetsData = data;
		res.locals.sheetsHeader = fillEmptyKey(sheets["Request Windows"].sheet.headerValues);
		next();
	}else{
		res.status(500).send("Couldn't connect");
	}
};
const parseWorkbook = async (req, res, next) => {
	try {
		const eupgradeFileInfo = createFile("/eupgEligibilityWindow", "json");
		const { sheetsData, sheetsHeader } = res.locals;
		const [ eUpgradeData ] = sheetsData;
		
		let bookinClass;
		let zone;
		const eupgradeJSON = {};

		sheetsHeader.forEach((headerVal) =>{
			const [bookingLetter, bookingClass] = headerVal.split("-");
			if(bookingLetter && bookingClass){
				eupgradeJSON[bookingLetter] = eupgradeJSON[bookingLetter] || {};
				eupgradeJSON[bookingLetter][bookingClass] = {};
			}
		})
		
		eUpgradeData.forEach((row, rowIndex) => {
			bookinClass = eUpgradeData[rowIndex][0] || bookinClass;
			zone = eUpgradeData[rowIndex][1] || zone;
			row.forEach((cell, cellIndex) => {
				if(rowIndex >= 2 && cellIndex >= 2  && Number.isNaN(parseInt(cell)) && cell !== "NA"){
					const key = cell;
					const value = eUpgradeData[rowIndex][cellIndex + 1];
					if(key && value && key !== "#REF!" && value !== "#REF!") {
						const [bookingLetter, upgradeClass] = sheetsHeader[cellIndex].split("-");
						const upgradedClassObj = eupgradeJSON[bookingLetter][upgradeClass];
						upgradedClassObj[bookinClass] = upgradedClassObj[bookinClass] ? upgradedClassObj[bookinClass] : {};
						upgradedClassObj[bookinClass][zone] = upgradedClassObj[bookinClass][zone] ? upgradedClassObj[bookinClass][zone] : {};
						if(value !== "NA" && value !== ""){
							upgradedClassObj[bookinClass][zone][key] = value;
						}
						console.log(bookinClass, " - ", zone, " - ", key, " = ", upgradedClassObj[bookinClass][zone][key]);
					}
				}
			});
		});

		writeFile(eupgradeFileInfo.fullPath, JSON.stringify(eupgradeJSON, null, 4), "utf-8");
		res.send({
			filename: eupgradeFileInfo,
		});
	} catch (err) {
		console.log("err = ", err);
		res.send({
			error: err,
		});
	}
};

module.exports = {
	getSheetsData,
	parseWorkbook,
};
