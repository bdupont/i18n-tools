var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var configAuth = include('config/auth');
var AWS = require('aws-sdk');
var HttpProxyAgent = require('http-proxy-agent');

AWS.config.update({
	region: "us-west-2",
	endpoint: "http://dynamodb.us-west-2.amazonaws.com",
	accessKeyId: "AKIAJERXAOGDYTAMWY4Q",
	secretAccessKey: "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",
	/* httpOptions: {
	    agent: HttpProxyAgent('http://proxyeast.aircanada.ca:8080')
	} */
});
var dynamodb = new AWS.DynamoDB();
var docClient = new AWS.DynamoDB.DocumentClient();

module.exports = function (passport) {
	// used to serialize the user for the session
	passport.serializeUser(function(user, done) {
		done(null, user);
	});

	// used to deserialize the user
	passport.deserializeUser(function(id, done) {
		done(null, id);
		/* User.findById(id, function (err, user) {
		    done(err, user);
		}); */
	});
	passport.use(
		new GoogleStrategy({
			clientID: configAuth.googleAuth.clientID,
			clientSecret: configAuth.googleAuth.clientSecret,
			callbackURL: configAuth.googleAuth.callbackURL,
		},
		(token, refreshToken, profile, done) => {
			process.nextTick(function () {
				console.log('profile = ', profile);
				var params = {
					TableName: "Users",
					KeyConditionExpression: "#email = :email",
					ExpressionAttributeNames: {
						"#email": "email"
					},
					ExpressionAttributeValues: {
						":email": profile.emails[0].value
					}
				};


				docClient.query(params, function (err, data) {
					if (err) {
						console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
					} else {
						var item = {
							"email": profile.emails[0].value,
							"access": false,
							"admin": false
						};
						if (data.Count) {
							done(null, data.Items[0])
						} else {
							var putParams = {
								TableName: "Users",
								Item: item
							};
							docClient.put(putParams, function (err, data) {
								if (err) {
									console.error("Unable to add item. Error JSON:", JSON.stringify(err, null, 2));
								} else {
									done(null, item);
								}
							});
						}
					}
				});

			});
		}
	));
};