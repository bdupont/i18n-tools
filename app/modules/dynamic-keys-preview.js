/* eslint-disable prettier/prettier */
/* eslint-disable function-paren-newline */
const requestPromise = require("request-promise-native");
// const { googleDocs, csvKeysLangOnly } = include("config/app.json");

// const { getAirportData } = include("services/locations.js");
const get = (path, extraOptions) => {
	const options = {
		uri: path,
		...extraOptions,
	};
	return requestPromise(options);
};

const getCitylist = () => {
	const locationPromise = get("https://www.aircanada.com/content/aircanada-config/ca/en/location.html");
	return async () => {
		const citylist = await locationPromise;
		return citylist;
	}
}

const citylistPromise = getCitylist();
const getRegion = async ({ country }) => {
	console.log("country = ", country);
	let region = "";
	const citylist = JSON.parse(await citylistPromise());
	citylist.markets.forEach(market => {
		if(market.countries.indexOf(country.toUpperCase()) !== -1){
			region = market.code.toLowerCase();
		}
	});
	if(!region){
		region = "int";
	}
	return region;
};
const getCityData = async airportCode =>{
	let data;
	const citylist = JSON.parse(await citylistPromise());
	citylist.countries.forEach(country => {
		if(country.states){
			country.states.forEach(state => {
				state.cities.forEach(city => {
					city.airports.forEach(airport => {
						if(airport.code === airportCode) {
							data = {
								airport: airport.code.toLowerCase(),
								country: country.code.toLowerCase(),
								state: state.code.toLowerCase(),
							};
						}
					});
				});
			});
		}else{
			country.cities.forEach(city => {
				city.airports.forEach(airport => {
					if(airport.code === airportCode) {
						data = {
							airport: airport.code.toLowerCase(),
							country: country.code.toLowerCase(),
						};
					}
				});
			});
		}
	});
	data.region = await getRegion(data);
	return data;
};

const getCityObj = async airport => {
	const cityData = await getCityData(airport.toUpperCase());
	return cityData;
};
const getObjPerKey = (jsonData, reg) => {
	const arr = Object.entries(jsonData)
		.filter(([key]) => reg.test(key))
		.map(([key, val]) => ({
			key,
			val,
		}));
	return arr;
};

const dynamicKeysExports = {
	getData: async (req, res) => {
		try {
			if(req.body.origin && req.body.destination){
				const subdomain = req.body.env === "PROD" ? "www" : req.body.env.toLowerCase();
				const path = `https://${subdomain}.aircanada.com/bin/fc`;
				const [origin, destination] = await Promise.all([getCityObj(req.body.origin), getCityObj(req.body.destination)]);
				const options = {
					qs: {
						input: JSON.stringify({
							c: req.body.country || "ca",
							l: req.body.language || "en",
							od: [
								{
									d: origin,
									o: destination,
								},
							]
						}),
						type: "v2"
					},
				};

				if(subdomain !== "www") {
					options.headers = {
						Authorization: "Basic Ym9iYnlkOmdlUndOUE5V"
					};
				}

				const data = await get(path, options);
				const jsonData = JSON.parse(data);

				const fareHighlights = getObjPerKey(jsonData, /^avail.fare.carousel.text/);
				const ulcc = getObjPerKey(jsonData, /^ulcc./);

				res.send({
					fareHighlights,
					ulcc,
				});	
			}			
		} catch (err) {
			console.log("err = ", err);
		}
	}
};


module.exports = dynamicKeysExports;
