/* eslint-disable prettier/prettier */
/* eslint-disable function-paren-newline */
const fs = require("fs-extra");
const { promisify } = require("util");

const writeFile = promisify(fs.writeFile);
// eslint-disable-next-line no-unused-vars
const { googleDocs, csvKeysLangOnly } = include("config/app.json");

const { getAirportData } = include("services/locations.js");
const { SheetDoc } = include("services/google-sheets.js");

const formatSheetsData = include("services/format-sheets-data.js");
const Web = include("classes/web.js");
const WebGrid = include("classes/web-grid.js");
const Mobile = include("classes/mobile.js");


// const { dynamicKeys } = include("config/app.json");
const { labelsValidator } = include("services/template-render.js");
// const { createFile, createZip, deleteDataFiles } = include("services/files.js");
const { createFile, createZip } = include("services/files.js");
const { JSONtoCSV } = include("services/generic-helpers.js");
const cache = {};
const ONE_HOUR = 60 * 60 * 1000;


const paddingOfResources = (resources) => {
	const arr = [];
	const pad = "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@";
	Object.values(resources).forEach(cabin => {
		Object.values(cabin).forEach(bookingClass => {
			const arrLenghts = bookingClass.map(bC => bC.en.split("@").length); 	 
			const maxLength = Math.max(...arrLenghts);
			bookingClass.forEach((resource) => {
				Object.entries(resource).forEach(([key, val]) => {
					if (key !== "key") {
						resource[key] += pad.substr(0, maxLength - val.split("@").length);
					}
				});
				arr.push(resource);
			});
		});
	});
	return JSONtoCSV(arr);
};

const FareBenefits = function(arr){
	this.fareBenefits = arr;
};

const getSheetsData = async (req, res, next) => {
	if(req.body.dynamicKeys) {
		const now = Date.now();
		if(cache[req.body.dynamicKeys] && (now - cache[req.body.dynamicKeys].timestamp <= ONE_HOUR)){
			console.log("cache");
			res.locals.sheetsData = cache[req.body.dynamicKeys].data;
			next();
		}else{
			console.log("non-cache");
			const sheetDoc = new SheetDoc(googleDocs[req.body.dynamicKeys]);
			await sheetDoc.connect();
			sheetDoc.addSheets(sheetDoc);
			const { sheets } = sheetDoc;
			try {
				console.time("parseAll");
				const promises = [
					sheetDoc.keyMappingPerLanguage(sheets.Translations.id, "googlesheet"),
					sheetDoc.keyMapping(sheets.Translations.id, "Key", "Icon"),
					sheetDoc.keyMapping(sheets.Translations.id, "Key", "Simplifed Icon"),
					sheetDoc.keyMapping(sheets.Zones.id, "Zone", "Country Code"),
					sheetDoc.keyMapping(sheets["Booking Class"].id, "Name", "Key"),
					sheetDoc.keyMapping(sheets.Countries.id, "en", "Country Code"),
					sheetDoc.keyMapping(sheets["States - Provinces"].id, "en", "State / Provinces Code"),
					sheetDoc.keyMapping(sheets.Devices.id, "Name", "Value"),
					sheetDoc.keyMapping(sheets.Features.id, "Name", "Value"),
					sheetDoc.parseCells(sheets.Rules.id),
					sheetDoc.parseCells(sheets["Rules - Basic Dialog"].id, "ulcc"),
					getAirportData("https://www.aircanada.com/content/aircanada-config/ca/en/location.html", "ENGLISH"),
					getAirportData("https://www.aircanada.com/content/aircanada-config/ca/fr/location.html", "FRENCH"),
				];
				const data = await Promise.all(promises);
				console.timeEnd("parseAll");
				res.locals.sheetsData = formatSheetsData(data);
				cache[req.body.dynamicKeys] = {
					data: res.locals.sheetsData,
					timestamp: Date.now(),
				}
				next();
			} catch (error) {
				console.log("error = ", error);	
			}		
		}
	}else{
		res.status(500).send("Couldn't connect");
	}
};
const parseWorkbook = async (req, res, next) => {
	if (req.body.dynamicKeys) {
		try {
			console.time("dynamicKeys");
			const fareResourcesFileInfo = createFile("farehighlights-resources", "csv");
			const ulccResourcesFileInfo = createFile("ulcc-resources", "csv");
			const fareGridResourcesFileInfo = createFile("farehighlightsGrid-resources", "csv");

			const fareInfo = new Web("details", "json", res);
			const ulccInfo = new Web("ulcc", "json", res);
			const fareInfoGrid = new WebGrid("detailsv2", "json", res);
			const mobileInfo = new Mobile("dynamic-keys-mobile", "json", res);
			console.timeEnd("dynamicKeys");

			await Promise.all([
				fareInfo.addDynamicData("fareHighlights"),
				ulccInfo.addDynamicData("basicDialogs"),
				fareInfoGrid.addDynamicData("fareHighlightsGrid"),
				mobileInfo.addDynamicData("fareHighlights")
			]);

			const [ fareInfoJSON, ulccInfoJSON, fareInfoGridJSON, mobileInfoJSON] = await Promise.all([
				fareInfo.processRules("fareHighlights"),
				ulccInfo.processRules("basicDialogs"),
				fareInfoGrid.processRules("fareHighlightsGrid"),
				mobileInfo.processRules("fareHighlights", "fareBenefits"),
			]);

			await Promise.all([
				writeFile(fareInfo.fileInfo.fullPath, JSON.stringify(fareInfoJSON, null, 4), "utf-8"),
				writeFile(ulccInfo.fileInfo.fullPath, JSON.stringify(ulccInfoJSON, null, 4), "utf-8"),
				writeFile(fareInfoGrid.fileInfo.fullPath, JSON.stringify(fareInfoGridJSON, null, 4), "utf-8"),
				writeFile(mobileInfo.fileInfo.fullPath, JSON.stringify({ fareBenefits: mobileInfoJSON }, null, 4), "utf-8"),
				writeFile(fareResourcesFileInfo.fullPath, JSONtoCSV(fareInfo.data.resources), "utf-8"),
				writeFile(ulccResourcesFileInfo.fullPath, JSONtoCSV(ulccInfo.data.resources), "utf-8"),
				writeFile(fareGridResourcesFileInfo.fullPath, paddingOfResources(fareInfoGrid.data.resources), "utf-8"),
				// writeFile(fareGridResourcesFileInfo.fullPath, JSONtoCSV(fareInfoGrid.data.resources), "utf-8"),
			]);

			const filesList = [
				fareInfo.fileInfo.fullPath,
				ulccInfo.fileInfo.fullPath,
				mobileInfo.fileInfo.fullPath,
				fareInfoGrid.fileInfo.fullPath,
				fareResourcesFileInfo.fullPath,
				ulccResourcesFileInfo.fullPath,
				fareGridResourcesFileInfo.fullPath,
				...mobileInfo.splitDataIntoFile("fareHighlights.fareBenefits", FareBenefits)
			];
			const zipFile = await createZip(filesList);
			// deleteDataFiles(filesList);

			res.send({
				missingLabels: labelsValidator.get(),
				filename: {
					name: "asfd" || zipFile.name,
				},
			});
		} catch (err) {
			console.log("err = ", err);
			res.send({
				error: err,
			});
		}
	}
};

module.exports = {
	getSheetsData,
	parseWorkbook,
};
