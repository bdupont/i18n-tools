/* eslint-disable prettier/prettier */
const SheetsData = include("classes/sheets-data");
const Prefix = include("classes/prefix");
const { isEditionSpecific, objectifyEdition, i18nLinks } = include("services/editions");
const { getRender, getRuleObj } = include("services/template-render");
const { stripRegionType } = include("services/strip-region-type");
const getLanguagesFromObject = include("services/get-languages-from-object");
const removeEmpty = include("services/remove-empty");
const getPrefix = include("services/get-prefix");
const setDefaultKey = include("services/set-default-key");
const setKey = include("services/set-key");

const { editionsKeys } = include("config/app.json");

const fixRender = code => {
	const reg = /<div class="ulcc_feature_text">[\s]*?<\/div>/g;
	const regNote = /<div class="ac-ulcc-footer-note">[\s\S]*?<\/div>/;
	const extraNoteMatches = code.match(regNote);
	const [extraNote] = extraNoteMatches || [""];
	const html = code.replace(regNote, "");
	const splitStr = "<div class=\"row ac-ulcc-content\">";
	const ulccContent = html.split(splitStr);
	const ulccFixed = ulccContent.filter(content => {
		const matches = content.match(reg);
		return !matches || matches.length < 2;
	});

	return ulccFixed.join(splitStr) + extraNote;
};

class Web extends SheetsData {
	constructor(fileName, fileExt, { locals: { sheetsData }, }) {
		super(sheetsData, fileName, fileExt);
		this.data = {
			dynamics: {
				keys: {},
				mappings: [],
			},
			resources: [],
		};
	}

	processRulesData(data) {
		const self = this;
		if (data.id !== "fareHighlightsGrid") {
			data.template.forEach((templateData, templateIndex) => {
				// deep clone of rulesData
				data.rulesData.forEach(rule => {
					const _obj = self.rulesDataForEachWeb(data, rule, templateData, templateIndex);
					Object.assign(data, _obj);
				});
			});
			return data;
		}
		return data;
	}

	rulesDataForEachWeb(data, rule, templateData, templateIndex) {
		const self = this;
		const processedData = self.data;
		const processedRule = {
			texts: [],
			compiledHTMLs: {
				default: objectifyEdition("googlesheet"),
				mobile: objectifyEdition("googlesheet"),
			},
		};
		const prefixData = new Prefix(self.bookingClass, self.devices, rule.keyData);
		for (let i = rule.start; i <= rule.end; i += 1) {
			processedRule.texts = processedRule.texts.concat([getRuleObj(data, i)]);
		}
		getLanguagesFromObject(self.translations).forEach(lang => {
			const render = {
				editionSpecificText: false,
				editionSpecific: isEditionSpecific(lang),
			};
			render.html = getRender(render, templateData, processedRule, lang, self.translations, self.icons);
			render.mobileViewHtml = getRender(render, templateData, processedRule, lang, self.translations, self.simplifedIcons);

			// remove and fix bad code due to different template source
			render.html = fixRender(render.html);
			render.mobileViewHtml = fixRender(render.mobileViewHtml);

			if ((render.editionSpecificText && render.editionSpecific) || !render.editionSpecific) {
				processedRule.compiledHTMLs.default[lang] = render.html;
				processedRule.compiledHTMLs.mobile[lang] = render.mobileViewHtml;
			}
		});
		Object.entries(processedRule.compiledHTMLs.default).forEach(([edition, value]) => {
			if (!isEditionSpecific(edition)) {
				editionsKeys[edition].forEach(editionKey => {
					const tempContent = {
						default: value && value.indexOf("replaceURL:/") !== -1 ? i18nLinks(processedRule.compiledHTMLs.default[editionKey.googlesheet] || value || "", editionKey.path) : processedRule.compiledHTMLs.default[editionKey.googlesheet] || "",
						mobile: value && value.indexOf("replaceURL:/") !== -1 ? i18nLinks(processedRule.compiledHTMLs.mobile[editionKey.googlesheet] || value || "", editionKey.path) : processedRule.compiledHTMLs.mobile[editionKey.googlesheet] || "",
					};
					processedRule.compiledHTMLs.default[editionKey.googlesheet] = tempContent.default;
					processedRule.compiledHTMLs.mobile[editionKey.googlesheet] = tempContent.mobile;
				});
			}
		});

		const origins = self.setRegion(rule.keyData[0]);
		const destinations = self.setRegion(rule.keyData[1]);

		if (origins.join("||").indexOf("#BADVALUE") === -1 && destinations.join("||").indexOf("#BADVALUE") === -1) {
			origins.forEach(origin => {
				destinations.forEach(destination => {
					const { suffix } = data.keys[templateIndex].aemKey;
					removeEmpty(processedRule.compiledHTMLs.default);
					removeEmpty(processedRule.compiledHTMLs.mobile);

					if (origin === "default") {
						if (data.processResources) {
							const prefix = getPrefix(data.keys[templateIndex].aemKey.defaultPrefix, prefixData);
							const strippedDestination = stripRegionType(destination).toUpperCase();
							const key = setDefaultKey(prefix, origin, strippedDestination, suffix);

							if (key.indexOf(".mobile.text") !== -1) {
								processedRule.compiledHTMLs.mobile.key = key;
								data.resources = [...(data.resources || []), processedRule.compiledHTMLs.mobile];
							} else {
								processedRule.compiledHTMLs.default.key = key;
								data.resources = (data.resources || []).concat([processedRule.compiledHTMLs.default]);
							}
						}
					} else {
						const prefix = getPrefix(data.keys[templateIndex].aemKey.prefix, prefixData);
						let key = setKey(prefix, origin, destination, suffix);

						if (destination.indexOf(".HK") !== -1 || destination.indexOf(".TW") !== -1) {
							key = key.replace(/.HK|.TW/gi, ".cn");
						}

						let mappingsIndex = processedData.dynamics.mappings.findIndex(mappedCompiledHTML => mappedCompiledHTML.en === processedRule.compiledHTMLs.default.en);
						if (mappingsIndex === -1) {
							mappingsIndex = processedData.dynamics.mappings.length;
							processedData.dynamics.mappings = [...processedData.dynamics.mappings, processedRule.compiledHTMLs.default];
						}

						processedData.dynamics.keys[key] = mappingsIndex;
						if (rule.keyData[2] && rule.keyData[2].toUpperCase() === "YES") {
							key = setKey(prefix, destination, origin, suffix);
							processedData.dynamics.keys[key] = mappingsIndex;
						}
					}
				});
			});
		}
		return data;
	}
}

module.exports = Web;
