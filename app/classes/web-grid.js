/* eslint-disable prettier/prettier */
const SheetsData = include("classes/sheets-data");
const Prefix = include("classes/prefix");
const GridRules = include("classes/grid-rules");

const { getRuleObj } = include("services/template-render");
const { editionsKeys } = include("config/app.json");
const { isEditionSpecific, objectifyEdition, i18nLinks } = include("services/editions");
const { stripRegionType } = include("services/strip-region-type");
const getLanguagesFromObject = include("services/get-languages-from-object");
const removeEmpty = include("services/remove-empty");
const getPrefix = include("services/get-prefix");
const setKey = include("services/set-key");

const updateResources = (resources = [], resourcesHTMLs, featuresHTMLs) => {
	let arr = resources.concat([resourcesHTMLs]);

	const indexOf = arr.map(e => e.key).indexOf(featuresHTMLs.key);
	if(indexOf !== -1){
		const resourcesLen = arr[indexOf].en.split("@").length;
		const featuresHTMLsLen = featuresHTMLs.en.split("@").length;
		if(featuresHTMLsLen > resourcesLen){
			console.log("featuresHTMLs.key = ", featuresHTMLs.key);
			arr[indexOf] = featuresHTMLs;
		}
	}else{
		arr = arr.concat([featuresHTMLs]);
	}
	return arr;
};

const setResources = (dest, cabin, fullCabin, bookingClass, resources, processedRule) => {
	const featuresHTMLs = { ...processedRule.featuresHTMLs };
	const resourcesHTMLs = { ...processedRule.resourcesHTMLs };

	featuresHTMLs.key = `fare.tray.${dest}.${fullCabin}.features`;
	resourcesHTMLs.key = `avail.fare.carousel.fare.tray.${dest}.${bookingClass}.content`;
	return updateResources(resources[dest][cabin], resourcesHTMLs, featuresHTMLs);
};

class WebGrid extends SheetsData {
	constructor(fileName, fileExt, { locals: { sheetsData } }) {
		super(sheetsData, fileName, fileExt);
		this.data = {
			dynamics: {
				keys: {},
				mappings: [],
				"features.mappings": [],
			},
			resources: [],
		};
	}	

	getFeatures(rule) {
		const self = this;
		let arr = [];
		rule.texts.forEach(text => {
			text.features.forEach(feature => {
				arr = [...arr, self.features[feature.trim()]];
			});
		});
		return arr;
	}

	getFeaturesLabels() {
		const self = this;
		const obj = {};
		getLanguagesFromObject(self.translations).forEach(lang => {
			const render = {
				editionSpecificText: false,
				editionSpecific: isEditionSpecific(lang),
			};
			if ((render.editionSpecificText && render.editionSpecific) || !render.editionSpecific) {
				obj[lang] = {};
				Object.entries(self.features).forEach(([key, feature]) => {
					const translation = self.translations[lang][`Features.${key}`] || key;
					const icon = self.icons[`Features.${key}`] || "";
					obj[lang][feature] = `<span class="ac-icon ${icon}"></span> <span>${translation}</span>`;
				});
			}
		});
		return obj;
	};

	processRulesData(data) {
		if (data.id === "fareHighlightsGrid") {
			const self = this;
			data.resources = {
				dom: {},
				tnb: {},
				nbm: {},
				sun: {}
			};
			data.rulesData.forEach(rule => {
				const _obj = self.rulesDataForEachGridWeb(data, rule);
				Object.assign(data, _obj);
			});
			self.data.dynamics["features.mappings"] = self.getFeaturesLabels();
			return data;
		}
	}

	rulesDataForEachGridWeb(data, rule) {
		const self = this;
		const processedData = self.data;
		const processedRule = {
			texts: [],
			compiledHTMLs: objectifyEdition("dynamicKeys"),
			featuresHTMLs: objectifyEdition("dynamicKeys"),
			resourcesHTMLs: objectifyEdition("dynamicKeys"),
		};
		const prefixData = new Prefix(self.bookingClass, self.devices, rule.keyData);

		for (let i = rule.start; i <= rule.end; i += 1) {
			processedRule.texts = [...processedRule.texts, getRuleObj(data, i)];
		}
		getLanguagesFromObject(self.translations).forEach(lang => {
			const tempLang = lang !== "zh_TR" ? lang.replace("_","").toLowerCase() : lang;
			const render = {
				editionSpecific: isEditionSpecific(lang),
			};
			render.html = new GridRules(processedRule, lang, self);
			if ((render.html.editionSpecificText && render.editionSpecific) || !render.editionSpecific) {
				processedRule.compiledHTMLs[tempLang] = render.html.rules;
				processedRule.featuresHTMLs[tempLang] = render.html.featuresResources;
				processedRule.resourcesHTMLs[tempLang] = render.html.rulesResources;
			}
		});
		Object.entries(processedRule.compiledHTMLs).forEach(([edition, value]) => {
			if (!isEditionSpecific(edition)) {
				const stringifyingVal = JSON.stringify(value);
				editionsKeys[edition].forEach(editionKey => {
					const tempContent = stringifyingVal && stringifyingVal.indexOf("replaceURL:/") !== -1 ? i18nLinks(stringifyingVal || "", editionKey.path) : "";
					if (tempContent) {
						processedRule.compiledHTMLs[editionKey.dynamicKeys] = tempContent;
					}
				});
			}
		});

		const origins = self.setRegion(rule.keyData[0], false);
		const destinations = self.setRegion(rule.keyData[1], false);

		if (origins.join("||").indexOf("#BADVALUE") === -1 && destinations.join("||").indexOf("#BADVALUE") === -1) {
			origins.forEach(origin => {
				destinations.forEach(destination => {
					const { suffix } = data.keys[0].aemKey;
					removeEmpty(processedRule.compiledHTMLs);
					removeEmpty(processedRule.featuresHTMLs);
					removeEmpty(processedRule.resourcesHTMLs);
					const prefix = getPrefix(data.keys[0].aemKey.prefix, prefixData);
					let key = setKey(prefix, origin, destination, suffix);
					let mappingsIndex = processedData.dynamics.mappings.findIndex(mappedCompiledHTML => Object.entries(mappedCompiledHTML.en).toString() === Object.entries(processedRule.compiledHTMLs.en).toString());

					if(key.indexOf("mobile.text") === -1){
						if (origin === "default") {
							const bookingClass = self.bookingClass[rule.keyData[5]];
							const bookingClassSplit = bookingClass.split(".");
							const cabin = bookingClassSplit[0]
								.replace("economy", "eco")
								.replace("premium", "prem")
								.replace("business", "exec");
							const fullCabin = bookingClass.indexOf("economy.latitude") !== -1 ? "eco.latitude" : cabin;

							const dest = stripRegionType(destination)
								.trim()
								.toLowerCase();
							const features = self.getFeatures(processedRule, self.features);
							processedData.dynamics[`${dest}.${cabin}.features`] = processedData.dynamics[`${dest}.${cabin}.features`] || [];
							processedData.dynamics[`${dest}.${cabin}.features`] = features.length > processedData.dynamics[`${dest}.${cabin}.features`].length ? features : processedData.dynamics[`${dest}.${cabin}.features`];
							if(dest === "dom") {
								processedData.dynamics[`tnb.${cabin}.features`] = processedData.dynamics[`${dest}.${cabin}.features`];
							}

							if(cabin.indexOf("eco") !== -1) {
								processedData.dynamics[`${dest}.eco.lat.features`] = processedData.dynamics[`${dest}.eco.features`];
								if(dest === "dom") {
									processedData.dynamics["tnb.eco.lat.features"] = processedData.dynamics[`${dest}.eco.features`];
								}
							}
							
							// Features list
							data.resources[dest][cabin] = setResources(dest, cabin, fullCabin, bookingClass, data.resources, processedRule);

							if(dest === "dom") {
								data.resources.tnb[cabin] = setResources("tnb", cabin, fullCabin, bookingClass, data.resources, processedRule);
							}
						} else {
							if (mappingsIndex === -1) {
								mappingsIndex = processedData.dynamics.mappings.length;
								processedData.dynamics.mappings = [...processedData.dynamics.mappings, processedRule.compiledHTMLs];
							}
							processedData.dynamics.keys[key] = mappingsIndex;
							if (rule.keyData[2] && rule.keyData[2].toUpperCase() === "YES") {
								key = setKey(prefix, destination, origin, suffix);
								processedData.dynamics.keys[key] = mappingsIndex;
							}
						}
					}
				});
			});
		}
		return data;
	}
};

module.exports = WebGrid;
