/* eslint-disable no-extend-native */
const fs = require("fs-extra");
const { promisify } = require("util");

const writeFile = promisify(fs.writeFile);
const { createFile } = include("services/files.js");

const { objectifyEdition } = include("services/editions");
const { retrieveTemplate } = include("services/template-render");
const { validateRow } = include("services/data-validator");
const renameProperty = include("services/rename-property");
const { dynamicKeys, regions } = include("config/app.json");
const getPropByString = include("services/get-prop-by-string");

const createRulesDataObj = ({ rules, keysValidation, id }) => {
	let rulesData = [];
	rules.forEach((row, index) => {
		if (validateRow(row, keysValidation)) {
			if (row[0]) {
				rulesData = [
					...rulesData,
					{
						start: index,
						end: index,
						texts: [],
						benefitsMobile: [],
						compiledHTMLs: objectifyEdition("googlesheet"),
						keyData: row.slice(0, 6),
					},
				];
			} else {
				rulesData[rulesData.length - 1].end = index;
			}
		} else {
			console.log(`${id} - row ${index} doesn't contain minimum data, won't be process`);
		}
	});
	return rulesData;
};

const filterDuplicateKeys = data => {
	if (Object.prototype.hasOwnProperty.call(data, "keys")) {
		const { keys } = data;
		Object.keys(keys).forEach(key => {
			if (key.indexOf("*zone*") !== -1) {
				const countryKey = key.replace(/\*zone\*/gi, "*country*");
				if (keys[countryKey]) {
					delete keys[key];
				}
			}
			const stripKey = key.replace(/\.\*zone\*/gi, "").replace(/\.\*country\*/gi, "");
			renameProperty(keys, key, stripKey);
		});
	}
	return data;
};

class SheetsData {
	constructor(data, fileName, fileExt) {
		Object.assign(this, data);
		this.fileInfo = createFile(fileName, fileExt, null, false);
	}

	async addDynamicData(key) {
		if (!this.dynamicData[key]) {
			this.dynamicData[key] = dynamicKeys[key];
		}

		const dynamicData = this.dynamicData[key];
		dynamicData.id = key;
		dynamicData.rules = this.rules[key];
		// console.log("dynamicData = ", dynamicData);
		// eslint-disable-next-line prettier/prettier
		const [template, rulesData] = await Promise.all([
			retrieveTemplate(dynamicData.keys),
			createRulesDataObj(dynamicData),
		]);

		dynamicData.template = template;
		dynamicData.rulesData = rulesData;
	}

	processRules(dynamicDataKey, key = "dynamics") {
		const self = this;
		if (dynamicDataKey) {
			this.data = { ...this.data, ...self.processRulesData(this.dynamicData[dynamicDataKey]) };
		} else {
			Object.values(this.dynamicData).forEach(data => {
				this.data = { ...this.data, ...self.processRulesData(data) };
			});
		}
		const finalData = filterDuplicateKeys(this.data[key] || this.data[dynamicDataKey] || {});
		return finalData;
	}

	splitDataIntoFile(keys, Construtor, nbOfEl = 3000) {
		const getEndIndex = (i, len) => {
			const endIndex = (i + 1) * nbOfEl - 1;
			return endIndex > len ? len : endIndex;
		};
		let filesList = [];
		const arr = getPropByString(this.dynamicData, keys);
		const arrLen = arr.length;
		const nbFiles = Math.floor(arrLen / nbOfEl);
		for (let i = 0; i <= nbFiles; i += 1) {
			const tempFilePath = this.fileInfo.fullPath.replace(".json", `-${i + 1}.json`);
			const startIndex = i * nbOfEl;
			const endIndex = getEndIndex(i, arrLen);
			const sliceArr = arr.slice(startIndex, endIndex);
			const fareBenefits = new Construtor(sliceArr);
			// {
			// 	fareBenefits: sliceArr,
			// };
			const fareBenefitsData = JSON.stringify(fareBenefits, null, 4);
			writeFile(tempFilePath, fareBenefitsData, "utf-8");

			// add to tempFilePath to filelist
			filesList = [...filesList, tempFilePath];
		}
		return filesList;
	}

	setRegion(regionStr, keepAll = false, returnDefault = true) {
		const self = this;
		const { zones, countriesCode, statesProvincesCode } = self;
		const mapRegionKey = regionVal => {
			const region = regionVal.trim();
			if (region === "Default") {
				return "default";
			}
			if (regions[region]) {
				return `region.${regions[region].code}`;
			}
			if (zones[region]) {
				return zones[region]
					.split(",")
					.map(country => `country.${country}.*zone*`)
					.join(",");
			}
			if (countriesCode[region]) {
				return `country.${countriesCode[region]}.*country*`;
			}
			if (statesProvincesCode[region]) {
				return `state.${statesProvincesCode[region]}`;
			}
			if (region.length === 3) {
				return `airport.${region}`;
			}
			return `${region}-#BADVALUE`;
		};
		if (regionStr === "ALL" || (regionStr === "Default" && !returnDefault)) {
			return !keepAll ? ["region.DOM", "region.TNB", "region.SUN", "region.INT"] : ["All"];
		}

		return regionStr
			.split(/[\n,]/gi)
			.map(mapRegionKey)
			.join(",")
			.split(",")
			.filter((region, index, selfRegion) => selfRegion.indexOf(region) === index)
			.map(region => region.trim());
	}
}

module.exports = SheetsData;
