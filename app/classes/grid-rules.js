const { isEditionSpecific } = include("services/editions.js");

class GridRules {
	constructor(rule, lang, { translations, icons, features }) {
		const self = this;
		self.rules = {};
		let featuresResources = [];
		let rulesResources = [];
		rule.texts.forEach(text => {
			const textContent = text.content.map(content => content.trim());
			const langMap = textContent.map(content => {
				const langOnly = lang.substr(0, 2);
				const translation = translations[lang][content] || "";
				if (isEditionSpecific(lang)) {
					if (translation) {
						self.editionSpecificText = true;
						return lang;
					}
				}
				return langOnly.toLowerCase();
			});

			textContent.forEach((content, i) => {
				const featuresText = text.features[i].trim();
				const featuresKey = `Features.${featuresText}`;
				const featureId = features[featuresText];
				const icon = icons[content] || "";
				const translation = translations[langMap[i]][content] || "";
				if (translation) {
					self.rules[featureId] = `<span class="ac-icon ${icon}"></span> <span>${translation}</span>`;

					featuresResources = featuresResources.concat([`<span class="ac-icon ${icons[featuresKey]}"></span> <span>${translations[lang][featuresKey]}</span>`]);
					rulesResources = rulesResources.concat([self.rules[featureId]]);
				}
			});
		});
		self.featuresResources = featuresResources.join("@");
		self.rulesResources = rulesResources.join("@");
	}
}

module.exports = GridRules;
