/* eslint-disable prettier/prettier */
const SheetsData = include("classes/sheets-data");
const { Location } = include("classes/location");
const { getRuleObj } = include("services/template-render");
const { stripRegionsType } = include("services/strip-region-type");
const { stripHTML } = include("services/strip-html");

class Mobile extends SheetsData {
	constructor(fileName, fileExt, { locals: { sheetsData }}) {
		super(sheetsData, fileName, fileExt);
		this.data = {
			fareBenefits: [],
		};
	}

	processRulesData(data) {
		const self = this;
		data.rulesData.forEach(rule => {
			const _arr = self.rulesDataForEachMobile(data, rule);
			data.fareBenefits = [...(data.fareBenefits || []), ..._arr];
		});
		return data;
	}

	rulesDataForEachMobile(data, rule) {
		const self = this;
		let fareBenefits = [];
		let benefitsMobile;
		const langs = ["en", "fr"];
		for (let i = rule.start; i <= rule.end; i += 1) {
			benefitsMobile = getRuleObj(data, i);
		}
		let origins = self.setRegion(rule.keyData[0], true);
		let destinations = self.setRegion(rule.keyData[1], true);

		origins = stripRegionsType(origins);
		destinations = stripRegionsType(destinations);

		if (origins.indexOf("#BADVALUE") === -1 && destinations.indexOf("#BADVALUE") === -1) {
			origins.forEach(origin => {
				destinations.forEach(destination => {
					langs.forEach(language => {
						const fromTemp = origin.replace(".*", "");
						const toTemp = destination.replace(".*", "");
						const from = new Location(self, language, fromTemp);
						const to = new Location(self, language, toTemp, fromTemp);
						const cabinName = self.translations[language][`Cabin.${rule.keyData[5]}`] || rule.keyData[5];
						if (from.name && to.name && ((rule.keyData[4] === "Desktop" && from.name !== "Default") || rule.keyData[4] === "Mobile")) {
							fareBenefits = [
								...fareBenefits,
								{
									language,
									from,
									to,
									cabinName,
									benefits: benefitsMobile.content.map(benefit => stripHTML(self.translations[language][benefit])),
								},
							];
						}
					});
				});
			});
		}
		return fareBenefits;
	}
}

module.exports = Mobile;
