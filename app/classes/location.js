const { regions } = include("config/app.json");

const getLocation = ({ countries, states, airports }, lang, location, originLocation) => {
	if (originLocation === "default" && regions[`${location} Destinations`]) {
		return regions[`${location} Destinations`].label[lang] || regions[`${location} Destinations`].label.en;
	}
	if (countries[lang][location]) {
		return countries[lang][location].name;
	}
	if (states[lang][location]) {
		return states[lang][location].name;
	}
	if (airports[lang][location]) {
		return airports[lang][location].name;
	}
	if (location === "All") {
		return "All";
	}
	if (location === "default") {
		return "Default";
	}
	return false;
};
const getCode = code => {
	if (code === "default") {
		return "";
	}
	if (regions[code]) {
		return regions[code].code;
	}
	return code;
};

class Location {
	constructor(sheets, language, code, originCode) {
		this.name = getLocation(sheets, language, code, originCode);
		this.code = getCode(code);
	}
}

class LocationList {
	constructor(en, fr, toolsFunc) {
		this.en = toolsFunc(en);
		this.fr = toolsFunc(fr);
	}
}

module.exports = {
	Location,
	LocationList,
};
