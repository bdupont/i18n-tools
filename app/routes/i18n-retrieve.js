// Express requirements
const express = require("express");

const app = express();
const { render, downloadFile } = include("modules/routing-functions.js");

// Google Sheets connection requirements
const { i18nRetrieve, i18nRetrieveTasks } = include("modules/i18n-retrieve.js");

// Basic routings for getting default page and setting the download link
app.get("/i18n-retrieve", render);
app.get("/i18n-retrieve-download", downloadFile);
app.get("/i18n-retrieve-tasks", i18nRetrieveTasks);

// Parsing POST request and return a success obj with the name of the create file to download (AJAX)
app.post("/i18n-retrieve-trigger", i18nRetrieve);

module.exports = app;
