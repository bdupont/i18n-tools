// Express requirements
const express = require("express");

const app = express();
const { downloadFile, render } = include("modules/routing-functions.js");

// Google Sheets connection requirements
const { getSheetsData, parseWorkbook } = include("modules/dynamic-keys.js");
const { getData } = include("modules/dynamic-keys-preview.js");

// Basic routings for getting default page and setting the download link
app.get("/dynamic-keys", render);
app.get("/dynamic-keys-preview", render);
app.get("/dynamic-keys-download", downloadFile);

// Parsing POST request and return a success obj with the name of the create file to download (AJAX)
app.post("/dynamic-keys-trigger", getSheetsData, parseWorkbook);
app.post("/dynamic-keys-preview-trigger", getData);

module.exports = app;
