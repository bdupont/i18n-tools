// Express requirements
const express = require("express");

const app = express();
const { render } = include("modules/routing-functions.js");

// Google Sheets connection requirements
// const acGoogleSpreadSheetsTools = include("modules/acGoogleSpreadSheets/dynamic-keys.js");

// Basic routings for getting default page and setting the download link
app.get("/fare-highlights", render);

module.exports = app;
