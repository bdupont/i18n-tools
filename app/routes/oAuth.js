const express = require("express");
// route middleware to make sure a user is logged in
const isLoggedIn = require("../modules/isLoggedIn");

module.exports = (app, passport) => {
	app.get("/profile", isLoggedIn, function(req, res) {
		res.render("Profile", {
			title: "Profile",
		});
	});

	// route for logging out
	app.get("/logout", function(req, res) {
		req.logout();
		res.redirect("/");
	});

	app.get(
		"/auth/google",
		passport.authenticate("google", {
			scope: ["profile", "email"],
		}));
	// the callback after google has authenticated the user
	app.get(
		"/auth/google/callback",
		passport.authenticate("google", {
			successRedirect: "/",
			failureRedirect: "/",
		})
	);
};
