// Express requirements
const express = require("express");

const app = express();
const { render, downloadFile } = include("modules/routing-functions");

// Google Sheets connection requirements
const { kiloMappings } = include("modules/kilo-mappings.js");

const multer = require("multer");

const upload = multer({
	dest: "uploads/",
});

// Basic routings for getting default page and setting the download link
app.get("/kilo-mapping", render);
app.get("/kilo-mapping-download", downloadFile);

// Parsing POST request and return a success obj with the name of the create file to download (AJAX)
app.post("/kilo-mapping-upload", upload.single("file"), kiloMappings);

module.exports = app;
