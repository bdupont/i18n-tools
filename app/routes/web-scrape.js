// Express requirements
const express = require("express");

const app = express();
const { render } = include("modules/routing-functions.js");

// Google Sheets connection requirements
const { scrapeData, retrieveImages, scrapeDailog } = include("modules/web-scrape.js");

// Basic routings for getting default page and setting the download link
app.get("/web-scrape", render);
// app.get("/i18n-create-download", downloadFile);

// Parsing POST request and return a success obj with the name of the create file to download (AJAX)
/*
	To get images, do the following:
	- run http://localhost:4545/web-scrape-get-trigger
	- run http://localhost:4545/web-scrape-get-images
*/
app.get("/web-scrape-trigger", scrapeData);
app.get("/web-scrape-get-images", retrieveImages);
app.get("/web-scrape-dailog", scrapeDailog);

module.exports = app;
