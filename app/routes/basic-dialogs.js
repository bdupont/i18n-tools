// Express requirements
// Express requirements
const express = require("express");

const app = express();
const { render } = include("modules/routing-functions.js");
// Basic routings for getting default page and setting the download link
app.get("/basic-dialogs", render);

module.exports = app;
