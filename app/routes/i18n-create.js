// Express requirements
const express = require("express");

const app = express();
const { render, downloadFile } = include("modules/routing-functions.js");

// Google Sheets connection requirements
const { parseWorkbook } = include("modules/i18n-create.js");
const { getGoogleDriveFiles } = include("services/google-drive.js");

// Basic routings for getting default page and setting the download link
app.get("/i18n-create", render);
app.get("/i18n-create-download", downloadFile);

// Parsing POST request and return a success obj with the name of the create file to download (AJAX)
app.post("/i18n-create-trigger", parseWorkbook);

app.get("/i18n-getFiles", getGoogleDriveFiles);

module.exports = app;
