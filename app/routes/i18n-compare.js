// Express requirements
const express = require("express");

const app = express();
const { render, downloadFile } = include("modules/routing-functions.js");

// Google Sheets connection requirements
const { parseWorkbook, compareData } = include("modules/i18n-compare.js");
const multer = require("multer");

const upload = multer({
	dest: "uploads/",
});

// Basic routings for getting default page and setting the download link
app.get("/i18n-compare", render);
app.get("/i18n-compare-download", downloadFile);

// Parsing POST request and return a success obj with the name of the create file to download (AJAX)
app.post("/i18n-compare-trigger", upload.array("envData", 5), parseWorkbook, compareData);

module.exports = app;
