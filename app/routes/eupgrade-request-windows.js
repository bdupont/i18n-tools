// Express requirements
const express = require("express");

const app = express();
const { downloadFile, render } = include("modules/routing-functions.js");

// Google Sheets connection requirements
const { getSheetsData, parseWorkbook } = include("modules/eupgrade-request-windows.js");

// Basic routings for getting default page and setting the download link
app.get("/eupgrade-request-windows", render);
app.get("/eupgrade-request-windows-download", downloadFile);

// Parsing POST request and return a success obj with the name of the create file to download (AJAX)
app.post("/eupgrade-request-windows-trigger", getSheetsData, parseWorkbook);

module.exports = app;
