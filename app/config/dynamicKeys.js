return {
	fareHighlights: {
		func: "processFareHighlightsRules",
		filePrefix: "i18n-fare-highlights",
	},
	basicDialogs: {
		func: "processBasicDialogsRules",
		filePrefix: "i18n-basic-dialogs",
	},
};
