const fs = require("fs-extra");
const { promisify } = require("util");

const readFile = promisify(fs.readFile);
// const hogan = require("hogan.js");
const Handlebars = require("handlebars");

const { keyValidator } = include("services/data-validator.js");
const { isEditionSpecific } = include("services/editions.js");
const labelsValidator = keyValidator();

const getRuleObj = ({ id, rules }, i) => {
	const ruleObj = {
		basicDialogs: {
			title: rules[i][6],
			compare1: rules[i][7],
			compare2: rules[i][8],
			compare3: rules[i][9],
		},
		fareHighlights: {
			content: rules[i][6].split(/[\n]/gi),
		},
		fareHighlightsGrid: {
			features: rules[i][7] ? rules[i][7].split(/[\n]/gi) : "",
			content: rules[i][8] ? rules[i][8].split(/[\n]/gi) : "",
		},
		fareHighlightsMobile: rules[i][6].split(/[\n]/gi),
	};
	return ruleObj[id];
};

const getRule = (translations, lang, rule) => {
	let txt = "";
	let editionSpecific = false;
	if (translations[lang][rule]) {
		if (isEditionSpecific(lang)) {
			editionSpecific = true;
		}
		txt = translations[lang][rule];
	} else if (translations[lang].langCode && translations[translations[lang].langCode][rule]) {
		txt = translations[translations[lang].langCode][rule];
	}
	return {
		text: txt,
		editionSpecific,
	};
};

const getRender = (render, templateData, rule, lang, translations, icons) => {
	const getIconWithParams = ruleText => {
		let iconsStr = icons[ruleText];
		if (iconsStr && templateData.config.overwriteIcon) {
			Object.entries(templateData.config.overwriteIcon).forEach(([key, val]) => {
				iconsStr = iconsStr.replace(key, val);
			});
		}
		return icons[ruleText];
	};
	const formatData = (ruleText, c) => `<div class="${c}">\n<span class="ac-icon ac-1x ${getIconWithParams(ruleText)}"></span>\n${ruleText}\n</div>`;

	Handlebars.registerHelper("hasNote", function(options) {
		const t = getRule(translations, lang, "Basic Footer Note");
		return t.text ? options.fn(this) : options.inverse(this);
	});

	Handlebars.registerHelper("getIcon", function() {
		let ruleText = this;
		ruleText = ruleText.trim();
		return icons[ruleText];
	});
	Handlebars.registerHelper("getTranslation", function(text) {
		const t = getRule(translations, lang, text);
		if (t.editionSpecific) {
			render.editionSpecificText = true;
		}
		return t.text;
	});
	Handlebars.registerHelper("getText", function() {
		let ruleText = this;
		ruleText = ruleText.trim();
		const t = getRule(translations, lang, ruleText);
		if (t.editionSpecific) {
			render.editionSpecificText = true;
		}
		if (!t.text) {
			labelsValidator.append(ruleText);
		}
		return t.text;
	});
	Handlebars.registerHelper("getNote", function() {
		let str = "";
		const t = getRule(translations, lang, "Basic Footer Note");
		if (t.editionSpecific) {
			render.editionSpecificText = true;
		}
		if (t.text) {
			str = `<div class="ac-ulcc-footer-note">${t.text}</div>`;
		}
		return str;
	});
	Handlebars.registerHelper("getIconWithParams", getIconWithParams);

	Handlebars.registerHelper("processComparison", function(context) {
		let ret = "";
		for (let i = 0, j = context.length; i < j; i += 2) {
			if (context[i] && context[i].compare1) {
				ret += "<tr>\n";
				ret += "<td>\n";
				ret += formatData(context[i].compare1, "content-one");
				ret += context[i + 1] && context[i + 1].compare1 ? formatData(context[i + 1].compare1, "content-two") : "";
				ret += "</td>\n";
				ret += "<td>\n";
				ret += formatData(context[i].compare2, "content-one");
				ret += context[i + 1] && context[i + 1].compare2 ? formatData(context[i + 1].compare2, "content-two") : "";
				ret += "</td>\n";
				ret += "</tr>\n";
			}
		}
		return ret;
	});

	return templateData.template(rule);
};

const retrieveTemplate = keys =>
	new Promise(async resolve => {
		const promises = keys.map(templateData => readFile(templateData.template, "utf8"));
		const datas = await Promise.all(promises);
		const templates = datas.map((templateHTML, i) => ({
			template: Handlebars.compile(templateHTML),
			config: keys[i],
		}));
		resolve(templates);
	});

module.exports = {
	getRender,
	getRuleObj,
	retrieveTemplate,
	labelsValidator,
};
