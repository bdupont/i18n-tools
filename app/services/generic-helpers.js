/* eslint-disable prettier/prettier */
/* eslint-disable no-param-reassign */
// const letterColumn = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "BB", "CC", "DD", "EE", "FF", "GG", "HH", "II", "JJ", "KK", "LL", "MM", "NN", "OO", "PP", "QQ", "RR", "SS", "TT", "UU", "VV", "WW", "XX", "YY", "ZZ"];
const { Languages } = include("services/editions.js");
const { editionsKeys } = include("config/app.json");

const regExpRules = {
	doubleBrackets: /\[\[([\s\S]+?)\]\]/gi,
	replacedoubleBrackets: /[[]]/gi,
};

const objIsEmpty = myObject => !Object.keys(myObject).length;
const isObject = value => value && typeof value === "object" && value.constructor === Object;

const reverse = str =>
	str
		.split("")
		.reverse()
		.join("");

const idify = id =>
	id
		.toLowerCase()
		.replace(/<[\s\S]+?>/gi, "")
		.replace(/[\r\n\s]/gi, "")
		.replace(/[()]/gi, "");

const idifyLabel = id => id.replace(regExpRules.replacedoubleBrackets, "");

const splitZoneKey = zoneStr => {
	let zones = zoneStr.split(/[\n,]{1}/g);
	const _overwrite = {
		"asia (pacific list)": "pacific",
		"europe (brazil list)": "EUROPEBR",
		"middle east (zone 220 countries)": "MIDDLEEASTBR",
		"australia (zone 340 countries)": "SOUTHWESTPACIFICBR",
		"africa (zone 230 countries)": "AFRICABR",
		brazil: "BR",
	};

	zones = zones
		.map(zone => {
			if (_overwrite[zone.toLowerCase()]) {
				return _overwrite[zone.toLowerCase()];
			}
			return zone.replace(/\s/g, "");
		})
		.filter(zone => !!zone && zone.charAt(0) !== "(");
	return zones;
};

const getDataPerlang = (ws, idIndex) => {
	const _data = new Languages();
	let _id;
	const _header = exports.setHeader(ws.getRow(1));
	ws.eachRow((row, rowIndex) => {
		if (rowIndex > 1) {
			_id = row.getCell(idIndex).value;
			row.eachCell((cell, cellIndex) => {
				const _lang = _header[cellIndex];
				if (_lang && _id) {
					_data[_lang][exports.idify(_id)] = cell.value;
				}
			});
		}
	});
	return _data;
};

const setHeader = row => {
	const _arr = {};
	row.eachCell((cell, cellIndex) => {
		const _getLangId = () => {
			if (cell.value === "English" || cell.value === "EN" || cell.value === "en") {
				return "en";
			}
			if (cell.value === "French" || cell.value === "FR" || cell.value === "fr") {
				return "fr";
			}
			if (cell.value === "Italian" || cell.value === "IT" || cell.value === "it") {
				return "it";
			}
			if (cell.value === "German" || cell.value === "DE" || cell.value === "de") {
				return "de";
			}
			if (cell.value === "Spanish" || cell.value === "ES" || cell.value === "es") {
				return "es";
			}
			if (cell.value === "Japanese" || cell.value === "JA" || cell.value === "JP" || cell.value === "jp" || cell.value === "ja") {
				return "ja";
			}
			if (cell.value === "Chinese" || cell.value === "Simplified Chinese" || cell.value === "ZH" || cell.value === "zh") {
				return "zh";
			}
			if (cell.value === "Traditional Chinese" || cell.value === "Chinese (traditional)" || cell.value === "ZH_tr" || cell.value === "ZH_TR" || cell.value === "zh_tr" || cell.value === "zh_TR") {
				return "zh_tr";
			}
			if (cell.value === "Korean" || cell.value === "korean" || cell.value === "KO" || cell.value === "ko") {
				return "ko";
			}
		};
		const _l = _getLangId();

		if (_l) {
			_arr[parseInt(cellIndex)] = _l;
		}
	});
	return _arr;
};

const getContent = (str, lang, labels, lowerCase = true) => {
	if (regExpRules.doubleBrackets.test(str)) {
		let finalStr;
		const _regResult = str.match(regExpRules.doubleBrackets);
		for (let i = 0, len = _regResult.length; i < len; i += 1) {
			let _labelID = _regResult[i].replace(regExpRules.replacedoubleBrackets, "");
			if (lowerCase) {
				_labelID = _labelID.toLowerCase();
			}
			finalStr = str.replace(_regResult[i], labels[lang][_labelID]);
		}
		return finalStr.replace(/[\r\n]/gi, "<br>").replace(/(>{1})(<br>)| (<br>)(<?)/gi, "$1");
	}
	return str || "";
};

const getContentById = (str, lang, labels) => labels[lang][str];

const getDefaultData = (ws, offsetRowIndex, offsetCellId = 0) => {
	const _data = new Languages();
	const _header = exports.setHeader(ws.getRow(1));
	ws.eachRow((row, rowIndex) => {
		if (rowIndex >= offsetRowIndex) {
			row.eachCell((cell, cellIndex) => {
				const _lang = _header[cellIndex];
				const _id = row.getCell(1 + offsetCellId).value;
				if (_lang && _id) {
					_data[_lang][_id] = cell.value;
				}
			});
		}
	});
	return _data;
};

const keyingfy = str => `key,${str}\n`;

const doubleQuotingQuote = str => {
	const reg = /"/gi;
	// eslint-disable-next-line prettier/prettier
	return str.replace(reg, "\"\"");
};

const arrayFlat = arr => arr.reduce((flat, toFlatten) => flat.concat(Array.isArray(toFlatten) ? arrayFlat(toFlatten) : toFlatten), []);

const replaceUndef = data => {
	for (let i = 0, len = data.length; i < len; i += 1) {
		data[i] = data[i] || "";
	}
	return data;
};
const arrayToObj = (data, keyIndex = 0, replaceUndefined = true) => {
	const obj = {};
	data.forEach((row, rowI) => {
		if (rowI >= 0 && row[keyIndex]) {
			obj[row[keyIndex]] = replaceUndefined ? replaceUndef(row.splice(1)) : row.splice(1);
		}
	});
	return obj;
};
const unique = (value, index, self) => self.indexOf(value) === index;

const padNumber = (num, size) => {
	const s = `0000000000000000000000${num}`;
	return s.substr(s.length - size);
};

const escapeRegExp = str => str.replace(/[-[]\/{}()*+?.\^$|]/g, "\\$&");

const allMarkets = () => {
	const a = ["key"];
	Object.entries(editionsKeys).forEach(([key, editions]) => {
		a.push(key);
		editions.forEach(edition => {
			a.push(edition.AEMi18n);
		});
	});
	return a;
};

const JSONtoCSV = (arr, columns = allMarkets(), delimiter = ",") => {
	const mappedArr = arr.map(obj =>
		columns.reduce((acc, key) => {
			const k = key === "zh_TR" ? key : key.toLowerCase().replace("_", "");
			return `${acc}${!acc.length ? "" : delimiter}"${!obj[k] ? "" : obj[k].replace(/[\n\r\t]/gi, "").replace(/["]/gi, "\"\"")}"`;
		}, ""));

	const a = [columns.join(delimiter), ...mappedArr];
	return a.join("\n");
};

const asyncForEach = async (array, callback) => {
	for (let index = 0; index < array.length; index += 1) {
		await callback(array[index], index, array);
	}
};

module.exports = {
	reverse,
	idify,
	idifyLabel,
	splitZoneKey,
	getDataPerlang,
	setHeader,
	getContent,
	getContentById,
	getDefaultData,
	keyingfy,
	doubleQuotingQuote,
	isObject,
	objIsEmpty,
	arrayFlat,
	arrayToObj,
	filter: {
		unique,
	},
	padNumber,
	escapeRegExp,
	JSONtoCSV,
	asyncForEach,
};
