const appConfig = include("config/app.json");
const langs = ["EN", "FR", "DE", "ES", "IT", "JA", "ZH", "ZH_TR", "KO"];

class Languages {
	constructor() {
		langs.forEach(lang => {
			this[lang] = {};
		});
	}
}

const editionsPerLang = () => {
	const obj = {
		DE: ["CH", "DE"],
		EN: ["AR", "AU", "CA", "CH", "CL", "CN", "CO", "DE", "DK", "ES", "FR", "HK", "IE", "IL", "IT", "KR", "JP", "MX", "NL", "NO", "PA", "PE", "SE", "UK", "US", "TW", "VE"],
		ES: ["AR", "CL", "CO", "ES", "MX", "PA", "PE", "US", "VE"],
		FR: ["CA", "FR", "CH"],
		IT: ["IT"],
		JA: ["JP"],
		ZH: ["CA", "CN"],
		ZH_TR: ["TW"],
		KO: ["KR"],
	};
	return obj;
};
const langsPerEdition = () => ({
	CA: ["en", "fr", "zh"],
	JP: ["en", "ja"],
	CN: ["en", "zh"],
	TW: ["en", "zh_tr"],
});

const isEditionSpecific = lang => /[a-z]{4}|[A-Z]{2}_[A-Z]{2}/.test(lang) && lang !== "zhtr";

const commaifyLangs = () => {
	const mappedLangs = langs.map(lang => (lang === "ZH_TR" ? "zh_TR" : lang.toLowerCase()));
	return mappedLangs.join(",");
};
const commaifyEditions = src => {
	let arr = [];
	Object.keys(src).forEach(edition => {
		let str = edition.toLowerCase();
		if (edition.length === 5) {
			str = `${edition.substring(0, 2)}_${str.substring(3).toUpperCase()}`;
		} else if (str.length === 4) {
			str = `${edition.substring(0, 2)}_${str.substring(2).toUpperCase()}`;
		}
		arr = [...arr, str];
	});
	return arr.join(",");
};
const objectifyEdition = key => {
	const obj = {};
	Object.entries(appConfig.editionsKeys).forEach(([lang, editionArr]) => {
		obj[lang] = null;
		editionArr.forEach(edition => {
			obj[edition[key]] = null;
		});
	});
	return obj;
};
const i18nLinks = (html, path) => html.replace(/replaceURL:\/parent/gi, `https://www.aircanada.com/${path.toLowerCase()}/aco/`);

module.exports = {
	editionsPerLang,
	langsPerEdition,
	isEditionSpecific,
	commaifyLangs,
	commaifyEditions,
	objectifyEdition,
	Languages,
	i18nLinks,
};
