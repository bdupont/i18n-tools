const stripHTML = txt => txt.replace(/<[\s\S]+?>/gi, "");

module.exports = {
	stripHTML,
};
