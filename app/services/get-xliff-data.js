/* eslint-disable function-paren-newline */
const converXliffToJSON = include("services/convert-xliff-to-json");
const get = include("services/get-promise");
const getRequestOptions = include("services/get-requests-options");

const getXliffData = async (url, dictionary, lang, edition = "", src) => {
	let body = "";
	try {
		const protocol = url.indexOf("https") !== -1 ? "https" : "http";
		console.log(url);
		const response = await get[protocol](
			url,
			getRequestOptions({
				src: "AEM",
				env: src,
			})
		);
		response.on("data", chunk => (body += chunk));
		await response.end;
		const data = converXliffToJSON(body);
		return {
			url,
			dictionary,
			lang,
			edition,
			data,
		};
	} catch (err) {
		console.log(url, " - err = ", err);
	}
};

module.exports = getXliffData;
