const removeEmpty = obj => {
	Object.entries(obj).forEach(([key, val]) => {
		if (val && typeof val === "object") removeEmpty(val);
		else if (!val) delete obj[key];
	});
};

module.exports = removeEmpty;
