const authConfig = include("config/auth.json");

const getRequestOptions = ({ env, src }) => {
	if (src === "AEM") {
		const auth = authConfig[env];
		return {
			rejectUnauthorized: false,
			headers: {
				Authorization: auth,
			},
		};
	}
};

module.exports = getRequestOptions;
