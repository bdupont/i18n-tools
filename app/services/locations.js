const request = require("request");

class City {
	constructor(cityName, cityCode, airportName, countryCode, countryName, regionName, regionAliases, stateCode, stateName) {
		this.cityName = cityName;
		this.cityCode = cityCode;
		this.airportName = airportName;
		this.countryCode = countryCode;
		this.countryName = countryName;
		this.regionName = regionName;
		this.regionAliases = regionAliases ? regionAliases.join("|") : "";
		this.stateCode = stateCode;
		this.stateName = stateName;
	}
}
class Country {
	constructor(countryName) {
		this.name = countryName;
	}
}
class State {
	constructor(stateName) {
		this.name = stateName;
	}
}

const getAirportData = (url, langStr) =>
	new Promise((resolve, reject) => {
		request(url, (err, resp, data) => {
			if (err) {
				reject(new Error(`err on ${url}`));
			}
			resolve({
				langStr,
				data: JSON.parse(data),
			});
		});
	});

const getCityList = data => {
	const _obj = {};
	data.data.countries.forEach(country => {
		if (country.cities) {
			country.cities.forEach(city => {
				city.airports.forEach(airport => {
					_obj[airport.code] = new City(city.name, city.code, airport.name, country.code, country.name, airport.region_name, airport.aliases);
				});
			});
		} else {
			country.states.forEach(state => {
				state.cities.forEach(city => {
					city.airports.forEach(airport => {
						_obj[airport.code] = new City(city.name, city.code, airport.name, country.code, country.name, airport.region_name, airport.aliases, state.code, state.name);
					});
				});
			});
		}
	});
	return _obj;
};

const getCountryList = data => {
	const _obj = {};
	data.data.countries.forEach(country => {
		_obj[country.code] = new Country(country.name);
	});
	return _obj;
};

const getStateList = data => {
	const _obj = {};
	data.data.countries.forEach(country => {
		if (country.states) {
			country.states.forEach(state => {
				_obj[state.code] = new State(state.name);
			});
		}
	});
	return _obj;
};

module.exports = {
	getAirportData,
	getCityList,
	getStateList,
	getCountryList,
};
