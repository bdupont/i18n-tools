const setDefaultKey = (prefix, origin, destination, suffixStr) => {
	const suffix = suffixStr ? `.${suffixStr}` : "";
	return `${prefix}.${destination}${suffix}`;
};
module.exports = setDefaultKey;
