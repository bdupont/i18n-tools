const getLanguagesFromObject = obj => Object.keys(obj);

module.exports = getLanguagesFromObject;