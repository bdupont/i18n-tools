const http = require("http");
const https = require("https");
const { promisify } = require("util");

http.get[promisify.custom] = function getAsync(url, options) {
	return new Promise((resolve, reject) => {
		http.get(url, options, response => {
			response.end = new Promise(resolve => response.on("end", resolve));
			resolve(response);
		}).on("error", reject);
	});
};
https.get[promisify.custom] = function getAsync(url, options) {
	return new Promise((resolve, reject) => {
		https
			.get(url, options, response => {
				response.end = new Promise(resolve => response.on("end", resolve));
				resolve(response);
			})
			.on("error", reject);
	});
};

const get = {
	http: promisify(http.get),
	https: promisify(https.get),
};

module.exports = get;
