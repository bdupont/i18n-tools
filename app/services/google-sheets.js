/* eslint-disable prettier/prettier */
/* eslint-disable class-methods-use-this */
const { GoogleSpreadsheet } = require("google-spreadsheet");

const { objIsEmpty } = include("services/generic-helpers.js");
const appConfig = include("config/app.json");
const gdCredentials = include("config/ac-gd.json");

class Sheet {
	constructor(sheet, index) {
		this.sheet = sheet;
		this.index = index;
		this.id = sheet.sheetId;
		this.data = "";
	}
}

class SheetDoc {
	constructor(sheetID) {
		this.doc = new GoogleSpreadsheet(sheetID);
		this.sheets = {};
		this.info = null;
	}

	addSheets() {
		const self = this;
		Object.values(self.doc.sheetsById).forEach((sheet, index) => {
			self.sheets[sheet.title] = new Sheet(sheet, index);
		});
	}

	async connect() {
		const self = this;
		try{
			await self.doc.useServiceAccountAuth(gdCredentials);
			await self.doc.loadInfo();
		}catch(err) {
			return err;
		}
	}

	async keyMapping(sheetId, key, val) {
		const self = this;
		const obj = {};
		const sheet = self.doc.sheetsById[sheetId];
		const rows = await sheet.getRows();
		rows.forEach(row => {
			if(row[key]){
				const trimKey = row[key].trim();
				obj[trimKey] = row[val];
			}
		});
		return obj;
	}

	keyMappingPerLanguage(sheetId, editionMappingKey, key = "Key") {
		const self = this;
		const keyMappingLang = (rows, keyId, val) => {
			const obj = {};
			rows.forEach(row => {
				if (row[val]) {
					if(row[keyId]) {
						const trimKey = row[keyId].trim();
						obj[trimKey] = row[val];
					}					
				}
			});
			return obj;
		};
		const keyMappings = async () => {
			const obj = {};
			const sheet = self.doc.sheetsById[sheetId];
			const rows = await sheet.getRows();
			Object.keys(appConfig.editionsKeys).forEach(lang => {
				const langCompact = lang === "zh_TR" ? "zhtr" : lang;
				obj[lang] = keyMappingLang(rows, key, langCompact);

				appConfig.editionsKeys[lang].forEach(edition => {
					const editionObj = keyMappingLang(rows, key, edition[editionMappingKey]);
					if (!objIsEmpty(editionObj)) {
						obj[edition[editionMappingKey]] = editionObj;
						obj[edition[editionMappingKey]].langCode = lang;
					}
				});	
			});
			return obj;
		};
		// return async function response;
		return keyMappings();
	}

	async parseCells(sheetId, sheetName, overWriteFilter = false) {
		const self = this;
		const arr = [];
		try{
			const sheet = self.doc.sheetsById[sheetId];
			const rows = await sheet.getRows();
			const headerCheck = (()=>{
				const check = {
					ulcc: sheet.headerValues[6]
				}
				const keyPos = sheet.headerValues.indexOf("key");
				return check[sheetName] || sheet.headerValues[keyPos] || sheet.headerValues[0];
			})();

			rows
				.filter(row => overWriteFilter || row[headerCheck])
				.forEach((row, rowI) => {
					sheet.headerValues.forEach((key, headerI) => {
						if(row[key]){
							const i = rowI;
							if(!arr[i]) {
								arr[i] = [];
							}
							arr[i][headerI] = row[key];
						}
					});
				});
			return arr;
		}catch (err) {
			console.log("err = ", err);
		}
	}

	async headerMapping(sheetId) {
		const self = this;
		let arr = [];
		try{
			const sheet = self.doc.sheetsById[sheetId];
			await sheet.loadHeaderRow();
			sheet.headerValues.forEach(cell => {
				const val = cell === "zh_tr" ? "zh_TR" : cell
				arr = [...arr, val];
			});
			return arr;
		}catch (err) {
			console.log("err = ", err);
		}
	}
}

module.exports = {
	SheetDoc,
};
