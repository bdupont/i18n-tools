const setKey = (prefix, origin, destination, suffixStr) => {
	const suffix = suffixStr ? `.${suffixStr}` : "";
	return `${prefix}.${origin}.${destination}${suffix}`.toLowerCase();
};

module.exports = setKey;
