const { filter } = include("services/generic-helpers.js");

const keyValidator = () => {
	let missingKeys = [];
	return {
		get: () => missingKeys.filter(filter.unique),
		append: key => {
			missingKeys = missingKeys.concat([key]);
			return missingKeys;
		},
	};
};

const validateRow = (row, keysValidation) => {
	const isNotUndefined = key => typeof row[key] !== "undefined";
	const filtered = keysValidation.or ? keysValidation.or.filter(isNotUndefined) : keysValidation.filter(isNotUndefined);
	return keysValidation.or ? filtered.length > 0 : keysValidation.length === filtered.length;
};

module.exports = {
	keyValidator,
	validateRow,
};
