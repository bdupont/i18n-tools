const NodeGoogleDrive = require("node-google-drive");

const { googleDriveFolder } = include("config/app.json");
const gdCredentials = include("config/ac-gd.json");

const getGoogleDriveFiles = async (req, res) => {
	const googleDriveInstance = new NodeGoogleDrive({
		ROOT_FOLDER: googleDriveFolder.i18n,
	});
	await googleDriveInstance.useServiceAccountAuth(gdCredentials);
	const listFilesResponse = await googleDriveInstance.listFiles(googleDriveFolder.i18n, null, false);
	const sheetsFiles = listFilesResponse.files.filter(file => file.mimeType === "application/vnd.google-apps.spreadsheet");

	res.send({ files: sheetsFiles });
};

module.exports = {
	getGoogleDriveFiles,
};
