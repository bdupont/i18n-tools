const xml2js = require("xml2js");

const parser = new xml2js.Parser();

const converXliffToJSON = xmlBody => {
	const obj = {};
	parser.parseString(xmlBody, (err, jsonData) => {
		if (jsonData && jsonData.xliff) {
			const sources = jsonData.xliff.file[0].body[0]["trans-unit"];
			if (sources) {
				sources.forEach(keyPair => {
					if (keyPair.source[0]._) {
						obj[keyPair.source[0]._] = keyPair.target[0]._;
					}
				});
			}
		}
	});
	return obj;
};

module.exports = converXliffToJSON;
