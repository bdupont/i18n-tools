/* eslint-disable prettier/prettier */
const { getCountryList, getStateList, getCityList } = include("services/locations");
const { LocationList } = include("classes/location")

const formatSheetsData = ([translations, icons, simplifedIcons, zones, bookingClass, countriesCode, statesProvincesCode, devices, features, rules, rulesBasic, locationsEN, locationsFR]) => {
	// Get countries, states, and airport both EN and FR
	const countries = new LocationList(locationsEN, locationsFR, getCountryList);
	const states = new LocationList(locationsEN, locationsFR, getStateList);
	const airports = new LocationList(locationsEN, locationsFR, getCityList);
	return {
		translations,
		icons,
		simplifedIcons,
		zones,
		bookingClass,
		countriesCode,
		statesProvincesCode,
		devices,
		features,
		rules: {
			fareHighlights: rules,
			fareHighlightsGrid: rules,
			basicDialogs: rulesBasic,
		},
		countries,
		states,
		airports,
		dynamicData: {},
	};
};

module.exports = formatSheetsData;
