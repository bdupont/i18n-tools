const getPropByString = (obj, propString) => {
	let prop;
	let i;
	let iLen;
	if (!propString) {
		return obj;
	}

	const props = propString.split(".");
	for (i = 0, iLen = props.length - 1; i < iLen; i += 1) {
		prop = props[i];

		const candidate = obj[prop];
		if (candidate !== undefined) {
			obj = candidate;
		} else {
			break;
		}
	}
	return obj[props[i]];
};

module.exports = getPropByString;
