const dateStamp = (includeSecond = false) => {
	const _d = new Date();
	const mm = _d.getMonth() + 1;
	const dd = _d.getDate();
	const h = _d.getHours();
	const m = _d.getMinutes();
	const s = _d.getSeconds();
	const time = includeSecond ? [h, m, s] : [h, m];
	const dateDigitStr = [_d.getFullYear(), (mm > 9 ? "" : "0") + mm, (dd > 9 ? "" : "0") + dd].join("");

	return [dateDigitStr, time.join("")].join("-");
};

const date = () => {
	const _d = new Date();
	const mm = _d.getMonth() + 1;
	const dd = _d.getDate();
	// const h = _d.getHours();
	// const m = _d.getMinutes();
	const dateDigitStr = [(mm > 9 ? "" : "0") + mm, (dd > 9 ? "" : "0") + dd].join("");

	return [dateDigitStr, _d.getFullYear()].join("-");
};

const dateStr = (lang = "EN", offsetMonth = 0) => {
	const month = {
		en: ["", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
		fr: ["", "janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"],
	};
	const _d = new Date();
	const mm = _d.getMonth() + 1 + offsetMonth;
	const dd = _d.getDate();
	const yyyy = _d.getFullYear();
	// const h = _d.getHours();
	// const m = _d.getMinutes();

	return lang === "EN" ? `${month.en[mm]} ${dd}, ${yyyy}` : `${month.fr[mm]} ${dd}, ${yyyy}`;
};

module.exports = {
	dateStamp,
	date,
	dateStr,
};
