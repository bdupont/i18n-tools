const stripRegionType = txt => txt.replace(/(country|state|airport|region|zone)./gi, "");
const stripRegionsType = data => data.map(stripRegionType);

module.exports = {
	stripRegionType,
	stripRegionsType,
};
