const renameProperty = (obj, oldName, newName) => {
	// Do nothing if the names are the same
	if (oldName !== newName) {
		if (Object.prototype.hasOwnProperty.call(obj, oldName)) {
			obj[newName] = obj[oldName];
			delete obj[oldName];
		}
	}
};

module.exports = renameProperty;
