const getPrefix = (prefix, prefixData) => {
	let prefixMapped = prefix;
	const regExp = /\{\{([\s\S]*?)\}\}/gi;
	const matches = prefix.match(regExp);
	if (matches) {
		matches.forEach(match => {
			const key = match.replace(/\{\{|\}\}/gi, "");
			if (key.charAt(0) === "." || key.charAt(0) === "_") {
				const keyPrefix = key.charAt(0);
				const strippedKey = key.substring(1);
				if (prefixData[strippedKey]) {
					prefixMapped = prefixMapped.replace(match, `${keyPrefix}${prefixData[strippedKey]}`);
				} else {
					prefixMapped = prefixMapped.replace(match, "");
				}
			} else {
				prefixMapped = prefixMapped.replace(match, prefixData[key] || "");
			}
		});
	}
	return prefixMapped;
};

module.exports = getPrefix;
