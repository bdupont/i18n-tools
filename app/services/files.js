const { promisify } = require("util");
const fs = require("fs-extra");
const archiver = require("archiver");

// const writeFile = promisify(fs.writeFile);
const readFile = promisify(fs.readFile);
const unlink = promisify(fs.unlink);
const { pathToDownload } = include("config/app.json");
const { commaifyLangs, keyingfy } = include("services/editions.js");
// const { commaifyLangs, keyingfy } = include("services/files.js");
const { dateStamp } = include("services/dates.js");
const { isObject } = include("services/generic-helpers");

/*
const writeFile = (path, data) => {
	const wStream = fs.createWriteStream(path);
	wStream.write(JSON.stringify(data, null, 4));
	wStream.end();
};
*/

const setFileName = (fileName, fileType, withDateStamp = true, extraDirectory = "") => {
	const d = dateStamp();
	fs.ensureDir(`${pathToDownload}${extraDirectory}`);
	const fName = withDateStamp ? `${fileName}-${d}.${fileType}` : `${fileName}.${fileType}`;
	return {
		fullPath: `${pathToDownload}${extraDirectory}${fName}`,
		directory: `${pathToDownload}${extraDirectory}`,
		name: fName,
	};
};
const createFile = (fileName, fileType, content = "", withDateStamp = true) => {
	const fileInfo = setFileName(fileName, fileType, withDateStamp);
	fs.ensureDir(pathToDownload);
	fs.writeFile(fileInfo.fullPath, content, err => {});
	return fileInfo;
};
const createCSV = (fileName, contentData) => {
	let content = contentData;
	if (!content && content !== null) {
		content = commaifyLangs();
		content = keyingfy(content);
	} else {
		content = "";
	}
	return createFile(fileName, "csv", content);
};
const createTxt = (fileName, content = "") => createFile(fileName, "txt", content);

const createArchive = fileInfo => {
	const output = fs.createWriteStream(fileInfo.fullPath);
	const archive = archiver("zip", {
		zlib: {
			level: 9,
		},
	});
	archive.pipe(output);
	archive.on("error", err => {
		throw err;
	});
	return archive;
};
// Do not change to arrow function
const appendToArchive = function(data, fileName) {
	let dataStr = data;
	if (isObject(data)) {
		dataStr = JSON.stringify(data, null, 4);
	}
	this.append(dataStr, {
		name: fileName,
	});
};

const retrieveDataFromFile = fullPath => readFile(fullPath, "utf-8");

const createZip = (filesList, fileName = "details") =>
	new Promise(resolve => {
		const file = setFileName(fileName, "zip");
		const archive = createArchive(file);
		filesList.forEach(filepath => {
			if (filepath.charAt(filepath.length - 1) === "/") {
				let newPath = filepath.split("/");
				newPath = newPath[newPath.length - 1] ? newPath[newPath.length - 1] : newPath[newPath.length - 2];
				archive.directory(filepath, newPath);
			} else {
				const fileDir = filepath.split("/");
				const filename = fileDir[fileDir.length - 1];
				archive.file(filepath, {
					name: filename,
				});
			}
		});
		archive.finalize().then(() => resolve(file));
	});

const deleteDataFiles = filesList => {
	filesList.forEach(filepath => {
		unlink(filepath);
	});
};

module.exports = {
	createZip,
	createCSV,
	createTxt,
	createFile,
	retrieveDataFromFile,
	deleteDataFiles,
	appendToArchive,
	setFileName,
};
