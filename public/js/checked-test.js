acModule.controller("checkedTest", function($scope, $http, $q) {
	var _new = $http({
		method: "get",
		url: "json/json-new.json",
	});
	var _old = $http({
		method: "get",
		url: "json/json-old.json",
	});
	$q.all([_new, _old]).then((_results) => {
		var _trim = (_tempStr) => {
			return _tempStr.replace(/[\s,]$/gi, "");
		};
		var _newData = _results[0].data;
		var _oldData = _results[1].data;
		var checked = document.querySelector("#checked");
		var _str = "";
		for (var zone in _newData.zone) {
			var _newR = _trim(_newData.policies[_newData.zone[zone]].extra.remarks);
			var _oldR = _trim(_oldData.policies[_oldData.zone[zone]].extra.remarks);
			var _same = (_newR === _oldR);
			_str += `<strong>${zone}</strong> = ${_same}<br>`;
			if (!_same) {
				_str += `_newR = ${_newR} - _oldR = ${_oldR}<br>`;
			}
		}
		checked.innerHTML = _str;
	});
});