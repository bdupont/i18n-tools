/* eslint-disable prefer-destructuring */
/* eslint-disable prettier/prettier */
// eslint-disable-next-line no-undef
const acModule = angular.module("acToolsApp", ["ngFileUpload", "ngAnimate", "toastr"]);
$("input[type='file']").change(function() {});

acModule.service("formService", function(Upload, $q) {
	this.upload = function(file, url, extraParams = {}) {
		const $spinner = document.querySelector("#spinner");
		const $steps = document.querySelector("#spinner .steps");
		const deferred = $q.defer();
		$spinner.style.opacity = "0.9";
		$spinner.style.display = "block";
		let _data = {
			file,
		};
		_data = Object.assign(extraParams, _data);
		return Upload.upload({
			url,
			data: _data,
		}).then(resp => {
			console.log(" complete ");
			$steps.innerHTML = "Completed - Files produced";
			$spinner.style.opacity = "0";
			$spinner.style.display = "none";
			deferred.resolve(resp.data);
			return deferred.promise;
		},
		resp => {
			$steps.innerHTML = "Error - Please try again";
			setTimeout(function() {
				$spinner.style.opacity = "0";
				$spinner.style.display = "none";
			}, 4000);
			deferred.reject(resp);
			return deferred.promise;
		},
		evt => {
			const progressPercentage = parseInt((100.0 * evt.loaded) / evt.total);
			$steps.innerHTML = `Uploading - ${progressPercentage} % `;
			if (progressPercentage === 100) {
				$steps.innerHTML = "Processing file - please wait";
			}
		});
	};
});

acModule.service("downloadService", function() {
	this.set = function(path) {
		document.getElementById("download-block").style.display = "block";
		document.getElementById("download-link").href = path;
	};
	this.notValid = function() {
		document.getElementById("download-block").style.display = "none";
	};
});

acModule.service("retrieveTasks", function($http){
	this.get = function() {
		return $http({
			method: "get",
			url: "/i18n-retrieve-tasks",
		})
	}
	this.convertToArr = function(data) {
		console.log("Object.values(data) = ", Object.values(data));
		return Object.values(data);
	}
});

acModule.controller("dynamicKeysController", function($scope, $http, $timeout, downloadService) {
	const $spinner = document.querySelector("#spinner");
	$scope.iframe = {
		show: false
	};
	$scope.edition = "CA";
	$scope.language = "EN";
	$scope.env = "PLY";
	$scope.version = "V2";
	$scope.retrieveData = function() {
		$spinner.style.opacity = "0.9";
		$spinner.style.display = "block";
		$http({
			method: "post",
			url: "/dynamic-keys-trigger",
			data: {
				dynamicKeys: document.getElementById("dynamicKeys").value,
			},
		}).then(function(data) {
			$spinner.style.opacity = "0";
			$spinner.style.display = "none";
			if (data.data.error && data.data.error.length) {
				$scope.errors = data.error;
			} else {
				$scope.errors = undefined;
				downloadService.set(`/dynamic-keys-download?fn=${data.data.filename.name}`);
			}
		});
	};
	$scope.retrievePreviewData = () => {
		console.log(" = ", {
			origin: $scope.origin,
			destination: $scope.destination,
			edition: $scope.edition,
			language: $scope.language,
			env: $scope.env,
		});
		$http({
			method: "post",
			url: "/dynamic-keys-preview-trigger",
			data: {
				origin: $scope.origin,
				destination: $scope.destination,
				edition: $scope.edition,
				language: $scope.language,
				env: $scope.env,
			}
		}).then((data) => {
			console.log("data.data = ", data.data);
			window.parent.fcData = data.data;
			$timeout(function(){
				$scope.iframe.path = "/iframe/dynamic-keys-preview.html";
				$scope.iframe.show = true;
				document.getElementById("dynamicKeysPreview").contentWindow.location.reload();
				$scope.$apply();
			});			
		}, function(err){
			console.log("err = ", err);
		});
	};
});

acModule.controller("i18nCreateController", function($scope, $http, downloadService) {
	$http.get("/i18n-getFiles")
		.then(function(data){
			console.log("data.data = ", data.data);
			const files = [
				{
					id: "",
					name: " - Select a sheet to process - "
				}
			].concat(data.data.files);
			$scope.files = files;
			console.log("$scope.files = ", $scope.files);
			$scope.retrieve = $scope.files[0];
		})
	const $spinner = document.querySelector("#spinner");
	$scope.retrieve = "";
	$scope.retrieveData = function() {
		console.log("$scope.retrieve = ", $scope.retrieve);
		if($scope.retrieve.id){
			$spinner.style.opacity = "0.9";
			$spinner.style.display = "block";
			$http({
				method: "post",
				url: "/i18n-create-trigger",
				data: {
					src: $scope.retrieve,
				},
			}).then(function(data) {
				$spinner.style.opacity = "0";
				$spinner.style.display = "none";
				if (data.data.error && data.data.error.length) {
					$scope.errors = data.error;
				} else {
					$scope.errors = undefined;
					downloadService.set(`/i18n-create-download?fn=${data.data.filename.name}`);
				}
			});
		}
	};
});

acModule.controller("i18nCompareController", function($scope, formService, downloadService) {
	const $spinner = document.querySelector("#spinner");
	$scope.upload = file => {
		$spinner.style.opacity = "0.9";
		$spinner.style.display = "block";
		formService.upload(file, "/i18n-compare-upload").then(data => {
			if (data.error && data.error.length) {
				$scope.errors = data.error;
			} else {
				$scope.errors = undefined;
				downloadService.set(`/i18n-compare-download?fn=${data.filename}`);
			}
		});
	};
});

acModule.controller("i18nRetrieveController", ($scope, $http, downloadService, retrieveTasks) => {
	const $spinner = document.querySelector("#spinner");
	$scope.retrieveData = src => {
		$spinner.style.opacity = "0.9";
		$spinner.style.display = "block";
		$http({
			method: "post",
			url: "/i18n-retrieve-trigger",
			data: {
				src,
			},
		}).then(data => {
			retrieveTasks.get().then(tasks => {
				$scope.$parent.tasks = retrieveTasks.convertToArr(tasks.data);
			});
			$spinner.style.opacity = "0";
			$spinner.style.display = "none";
			if (data.data.error && data.data.error.length) {
				$scope.errors = data.error;
			} else {
				$scope.errors = undefined;
				downloadService.set(`/i18n-create-download?fn=${data.data.filename.name}`);
			}
		});
	};
});

acModule.controller("i18nRetrieveTasksController", ($scope, retrieveTasks) => {
	retrieveTasks.get().then(tasks => {
		$scope.tasks = retrieveTasks.convertToArr(tasks.data);
	});
	setTimeout(() => {
		retrieveTasks.get().then(tasks => {
			$scope.tasks = retrieveTasks.convertToArr(tasks.data);
		});
	}, 5000);

});

acModule.controller("eupgradeRequestWindowsController", ($scope, $http, $timeout, downloadService) => {
	const $spinner = document.querySelector("#spinner");
	$scope.retrieveData = () => {
		$spinner.style.opacity = "0.9";
		$spinner.style.display = "block";
		$http({
			method: "post",
			url: "/eupgrade-request-windows-trigger",
			data: {
				gdocId: document.getElementById("gdocId").value,
			},
		}).then(function(data) {
			$spinner.style.opacity = "0";
			$spinner.style.display = "none";
			if (data.data.error && data.data.error.length) {
				$scope.errors = data.error;
			} else {
				$scope.errors = undefined;
				downloadService.set(`/eupgrade-request-windows-download?fn=${data.data.filename.name}`);
			}
		});
	};
});

acModule.controller("kiloMappingController", function($scope, formService, downloadService) {
	const $spinner = document.querySelector("#spinner");
	$scope.upload = file => {
		$spinner.style.opacity = "0.9";
		$spinner.style.display = "block";
		formService.upload(file, "/kilo-mapping-upload").then(data => {
			if (data.error && data.error.length) {
				$scope.errors = data.error;
			} else {
				$scope.errors = undefined;
				downloadService.set(`/kilo-mapping-download?fn=${data.filename}`);
			}
		});
	};
});



window.acUpdateIframeHeight = function(h) {
	document.getElementById("dynamicKeysPreview").height = h;
}