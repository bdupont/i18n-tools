(function () {
    var getParams = function (path) {
        var _params = {};
        var _paths = path.split("?");
        if (_paths[1]) {
            var _paramStr = _paths[1].split("&");
            _paramStr.forEach(function (element, index) {
                var _param = element.split("=");
                _params[_param[0]] = _param[1];
            });
        }
        return _params;
    };
    window.addEventListener("load", function () {
        var _params = getParams(location.href);
        if (_params.fn) {
            document.getElementById("fn").value = _params.fn;
            document.getElementById("downloadForm").action = "/" + _params.path;
            document.getElementById("downloadForm").submit();
        }
    });
})();